/**
 * Express router providing all routes
 *
 * @module routes
 */
const express = require("express");
const AdmZip = require("adm-zip");
const fs = require("fs");
const mime = require("mime");
const path = require("path");
const { QueryTypes } = require("sequelize");
const fetch = require("node-fetch");

const Ontology = require("./models/ontologies");
const filter = require("./util/createFilter");
const getImports = require("./util/getImports");
const db = require("./db");
const config = require("./config");

/**
 * Express router
 * @const
 * @namespace router
 */
const router = express.Router();

/**
 * Route that creates SQL query for Ontology Search Based on Lexical Tokens
 *
 * @function
 * @name lexicalTokens
 * @memberof module:routes~router
 * @inner
 * @async
 */
router.post("/lexicalTokens", async (req, res) => {
  const body = req.body;
  let query = "";
  let entities = 0;
  let scope = 1;

  if (body.entities.includes("classes")) entities = 1;
  if (body.entities.includes("properties")) entities += 2;
  if (body.entities.includes("individuals")) entities += 4;

  if (body.scope.includes("localName")) scope += 2;
  if (body.scope.includes("label")) scope += 4;
  if (body.scope.includes("comment")) scope += 8;

  //if matcher is word match
  if (body.matcher === "fulltext") {
    if (body.resource === "all") {
      query = `SELECT DISTINCT entities_search.storage_code FROM entities_search WHERE MATCH (localname2, label, comment) AGAINST ("${body.word}" IN NATURAL LANGUAGE MODE)`;
    } else {
      query = `SELECT DISTINCT entities_search.storage_code FROM entities_search, ontologies WHERE MATCH(localname2, label, comment) AGAINST ("${body.word}" IN NATURAL LANGUAGE MODE)`;
    }

    if (entities === 1) query += ` AND type="class"`;
    else if (entities === 2)
      query += ` AND (type="object_property" OR type="datatype_property" OR type="annotation_property")`;
    else if (entities === 4) query += ` AND type="individuals"`;
    else if (entities === 3)
      query += ` AND (type="class" OR type="object_property" OR type="datatype_property" OR type="annotation_property")`;
    else if (entities === 5)
      query += ` AND (type="class" OR type="individual")`;
    else if (entities === 7)
      query += ` AND (type="class" OR type="object_property" OR type="datatype_property" OR type="annotation_property" OR type="individual")`;
    else if (entities === 6)
      query += ` AND (type="object_property" OR type="datatype_property" OR type="annotation_property" OR type="individual")`;

    if (body.resource !== "all") {
      query += ` AND entities_search.storage_code=ontologies.storage_code AND ontologies.resource="${body.resource}"`;
    }
  }

  //if matcher is exact match
  if (body.matcher === "exact") {
    if (body.resource === "all") {
      query =
        "SELECT DISTINCT entities_search.storage_code FROM entities_search WHERE ";
    } else {
      query =
        "SELECT DISTINCT entities_search.storage_code FROM entities_search, ontologies WHERE ";
    }

    //only one type
    if (entities === 1) query += ` AND type="class"`;
    else if (entities === 2)
      query += ` AND (type="object_property" OR type="datatype_property" OR type="annotation_property")`;
    else if (entities === 4) query += ` AND type="individual"`;
    //class and
    else if (entities === 3)
      query += ` AND (type="class" OR type="object_property" OR type="datatype_property" OR type="annotation_property")`;
    else if (entities === 5)
      query += ` AND (type="class" OR type="individual")`;
    else if (entities === 7)
      query += ` AND (type="class" OR type="object_property" OR type="datatype_property" OR type="annotation_property" OR type="individual")`;
    //property and
    else if (entities === 6)
      query += ` AND (type="object_property" OR type="datatype_property" OR type="annotation_property" OR type="individual")`;

    //only one scope
    if (scope === 3) query += ` AND localname1="${body.word}"`;
    else if (scope === 5) query += ` AND label="${body.word}"`;
    else if (scope === 9) query += ` AND comment="${body.word}"`;
    //localname and
    else if (scope === 7)
      query += ` AND (localname1="${body.word}" OR label="${body.word}")`;
    else if (scope === 11)
      query += ` AND (localname1="${body.word}" OR comment="${body.word}")`;
    else if (scope === 15)
      query += ` AND (localname1="${body.word}" OR label="${body.word}" OR comment="${body.word}")`;
    //label and
    else if (scope === 13)
      query += ` AND (label="${body.word}" OR comment="${body.word}")`;

    if (body.resource !== "all") {
      query += ` AND entities_search.storage_code=ontologies.storage_code AND ontologies.resource="${body.resource}"`;
    }
  }

  query = query.replace("WHERE  AND ", "WHERE ");

  const storageCodes = await db.query(query, {
    plain: false,
    raw: true,
    type: QueryTypes.SELECT,
  });

  const result = await db.query(
    "SELECT * FROM ontologies WHERE storage_code IN(?)",
    {
      replacements: [storageCodes.map((item) => item.storage_code)],
      plain: false,
      raw: true,
      type: QueryTypes.SELECT,
    }
  );

  res.json(result);
});

/**
 * Route that selects all distinct resources from a database
 *
 * @function
 * @name resourceFilter
 * @memberof module:routes~router
 * @inner
 * @async
 */
router.get("/resourceFilter", async (req, res) => {
  const resources = await Ontology.aggregate("resource", "DISTINCT", {
    plain: false,
  });

  return res.status(200).json(resources);
});

/**
 * Route that selects downloads an ontology with all its imports as a ZIP
 *
 * @function
 * @name downloadWithImports
 * @memberof module:routes~router
 * @inner
 * @async
 */
router.post("/downloadWithImports/:storage_code", async (req, res) => {
  try {
    const zip = new AdmZip();
    const storage_code = req.params.storage_code;
    const imports = await getImports(storage_code);

    const ontologyResponse = await fetch(
      `${config.ontologiesURL}/${storage_code}.owl`,
      {
        method: "GET",
      }
    );
    const ontology = await ontologyResponse.text();

    zip.addFile(`${storage_code}.owl`, Buffer.alloc(ontology.length, ontology));

    for (item of imports) {
      let importedOntologyResponse = await fetch(
        `${config.ontologiesURL}/${item.imported}.owl`,
        {
          method: "GET",
        }
      );
      const importedOntology = await importedOntologyResponse.text();

      zip.addFile(
        `${item.imported}.owl`,
        Buffer.alloc(importedOntology.length, importedOntology)
      );
    }

    let zipFileContents = zip.toBuffer();

    const fileName = "ontologies.zip";
    const fileType = "application/zip";
    return res
      .status(200)
      .set({
        "Content-Disposition": `attachment; filename="${fileName}"`,
        "Content-Type": fileType,
      })
      .end(zipFileContents);
  } catch (e) {
    console.log(e);
  }
});

/**
 * Route that selects downloads individual ontologies
 *
 * @function
 * @name download
 * @memberof module:routes~router
 * @inner
 * @async
 */
router.post("/download/:storage_code", async (req, res) => {
  const storage_code = req.params.storage_code;

  const ontologyResponse = await fetch(
    `${config.ontologiesURL}/${storage_code}.owl`,
    {
      method: "GET",
    }
  );

  const ontology = await ontologyResponse.text();

  return res
    .status(200)
    .set({
      "Content-Disposition": `attachment; filename="${storage_code}.owl"`,
      "Content-Type": mime.getType("owl"),
    })
    .send(ontology);
});

/**
 * Route that selects downloads all currently selected ontologies as a ZIP
 *
 * @function
 * @name downloadAll
 * @memberof module:routes~router
 * @inner
 * @async
 */
router.post("/downloadAll", async (req, res) => {
  try {
    const zip = new AdmZip();
    const { storage_codes, page, metrics } = req.body;
    let metricsSelection = "";

    if (page === "1") {
      metricsSelection =
        "Selected resource: " +
        (metrics.selectedResource || "all") +
        "\n" +
        "Selected metrics: " +
        Object.keys(metrics)
          .filter((metrics) => metrics !== "selectedResource")
          .map((item) => {
            return `${item}: ${metrics[item]}`;
          })
          .join(" and ");
    }

    if (page === "2") {
      metricsSelection = `Selected resource: ${metrics.resourceFilter}\nWord: ${
        metrics.word
      } and Matcher: ${metrics.matcher} and Entities: ${metrics.entities.join(
        ","
      )} and Scope: ${metrics.scope.join(",")}`;
    }

    zip.addFile(
      `info.txt`,
      Buffer.alloc(metricsSelection.length, metricsSelection)
    );

    for (const item of storage_codes) {
      let ontologyResponse = await fetch(
        `${config.ontologiesURL}/${item}.owl`,
        {
          method: "GET",
        }
      );
      const ontology = await ontologyResponse.text();

      zip.addFile(`${item}.owl`, Buffer.alloc(ontology.length, ontology));
    }

    let zipFileContents = zip.toBuffer();

    const fileName = "ontologies.zip";
    const fileType = "application/zip";
    res.writeHead(200, {
      "Content-Disposition": `attachment; filename="${fileName}"`,
      "Content-Type": fileType,
    });
    return res.end(zipFileContents);
  } catch (e) {
    console.log(e);
    return res
      .status(413)
      .json({ message: "Cound not zip requested ontologies" });
  }
});

/**
 * Route that finds all storage codes based on selected filters
 *
 * @function
 * @name getStorageCodes
 * @memberof module:routes~router
 * @inner
 * @async
 */
router.post("/getStorageCodes", async (req, res) => {
  const body = req.body;

  const result = await Ontology.findAll({
    attributes: ["storage_code"],
    where: await filter(body),
  });
  return res.status(200).json(result);
});

/**
 * Route that finds all storage codes based on selected filters
 *
 * @function
 * @name getOntolologiesStats
 * @memberof module:routes~router
 * @inner
 * @async
 */
router.get("/getOntolologiesStats/:resource", async (req, res) => {
  const resource = req.params.resource;
  let numberOfOntologies;

  const stats = await db.query(
    `SELECT * FROM ontologies_stats WHERE resource = ?`,
    {
      replacements: [resource],
      type: QueryTypes.SELECT,
    }
  );

  if (resource === "all") {
    numberOfOntologies = await db.query(
      "SELECT COUNT(storage_code) AS numOfOntologies FROM ontologies",
      {
        type: QueryTypes.SELECT,
      }
    );
  } else {
    numberOfOntologies = await db.query(
      `SELECT COUNT(storage_code) AS numOfOntologies FROM ontologies WHERE resource = ?`,
      {
        replacements: [resource],
        type: QueryTypes.SELECT,
      }
    );
  }

  return res.json({ stats: stats, numOfOntologies: numberOfOntologies });
});

/**
 * Route that finds all ontologies based on selected filters
 *
 * @function
 * @name ontologies
 * @memberof module:routes~router
 * @inner
 * @async
 */
router.post("/ontologies", async (req, res) => {
  const body = req.body;
  // const { limit, skip } = req.query;
  Object.keys(body).forEach(
    (k) => !body[k] && body[k] !== undefined && delete body[k]
  );

  const result = await Ontology.findAll({
    where: await filter(body),
    // limit: parseInt(limit) || null,
    // offset: parseInt(skip) || null,
  });
  return res.status(200).json(result);
});

/**
 * Route that finds all ontologies based on storage codes
 *
 * @function
 * @name ontologiesByStorageCodes
 * @memberof module:routes~router
 * @inner
 * @async
 */
router.post("/ontologiesByStorageCodes", async (req, res) => {
  const storageCodes = req.body;
  const { limit, skip } = req.query;

  const result = await Ontology.findAll({
    where: {
      storage_code: storageCodes.map((storageCode) => storageCode.storage_code),
    },
    limit: parseInt(limit) || null,
    offset: parseInt(skip) || null,
  });
  return res.status(200).json(result);
});

module.exports = router;
