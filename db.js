/**
 * Ontology properties ranges
 *
 * @module database
 */
const Sequelize = require("sequelize");
const config = require("./config");

/**
 * @namespace db
 */

/**
 * Creates new sequelize database connection
 *
 * @function
 * @name sequelize
 * @memberof module:database~db
 * @inner
 */
const sequelize = new Sequelize(
  config.DB_NAME,
  config.DB_USER,
  config.DB_PASS,
  {
    host: config.DB_HOST,
    dialect: "mariadb",
    dialectOptions: {
      timezone: "Etc/GMT-1",
    },
  }
);

module.exports = sequelize;
