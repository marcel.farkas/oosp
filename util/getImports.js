/**
 * Ontology imports
 *
 * @module util
 */
const { QueryTypes } = require("sequelize");

const db = require("../db");

/**
 * @namespace imports
 */

/**
 * Searchis for ontology imports based on storage code
 *
 * @function
 * @name getImports
 * @memberof module:util~imports
 * @inner
 * @async
 */
getImports = async (storageCode) => {
  try {
    return await db.query(
      "SELECT imported FROM imported_ontologies WHERE importing = ?",
      {
        replacements: [storageCode],
        type: QueryTypes.SELECT,
      }
    );
  } catch (e) {
    console.log(e);
  }
};

module.exports = getImports;
