/**
 * Ontology properties ranges
 *
 * @module util
 */
const { QueryTypes } = require("sequelize");

const db = require("../db");

/**
 * @namespace ranges
 */

/**
 * Selects minimum and maximum values for every property of ontologies
 *
 * @function
 * @name getRanges
 * @memberof module:util~ranges
 * @inner
 * @async
 */
getRanges = async () => {
  return await db.query(
    `SELECT 
        MIN(classes) AS classes_min, MAX(classes) AS classes_max,
        MIN(classesAPIimport) AS classesAPIimport_min, MAX(classesAPIimport) AS classesAPIimport_max,
        MIN(object_properties) AS object_properties_min, MAX(object_properties) AS object_properties_max,
        MIN(object_propertiesAPIimport) AS object_propertiesAPIimport_min, MAX(object_propertiesAPIimport) AS object_propertiesAPIimport_max,
        MIN(datatype_properties) AS datatype_properties_min, MAX(datatype_properties) AS datatype_properties_max,
        MIN(datatype_propertiesAPIimport) AS datatype_propertiesAPIimport_min, MAX(datatype_propertiesAPIimport) AS datatype_propertiesAPIimport_max,
        MIN(instances) AS instances_min, MAX(instances) AS instances_max,
        MIN(instancesAPIimport) AS instancesAPIimport_min, MAX(instancesAPIimport) AS instancesAPIimport_max,
        MIN(imports) AS imports_min, MAX(imports) AS imports_max,
        MIN(axiomCount) AS axiomCount_min, MAX(axiomCount) AS axiomCount_max,
        MIN(logicalAxioms) AS logicalAxioms_min, MAX(logicalAxioms) AS logicalAxioms_max,
        MIN(GCICount) AS GCICount_min, MAX(GCICount) AS GCICount_max,
        MIN(hiddenGCICount) AS hiddenGCICount_min, MAX(hiddenGCICount) AS hiddenGCICount_max,
        MIN(axEquivalentClasses) AS axEquivalentClasses_min, MAX(axEquivalentClasses) AS axEquivalentClasses_max,
        MIN(axDisjoint) AS axDisjoint_min, MAX(axDisjoint) AS axDisjoint_max,
        MIN(axDisjointUnion) AS axDisjointUnion_min, MAX(axDisjointUnion) AS axDisjointUnion_max,
        MIN(axClassAssertion) AS axClassAssertion_min, MAX(axClassAssertion) AS axClassAssertion_max,
        MIN(axDifferent) AS axDifferent_min, MAX(axDifferent) AS axDifferent_max,
        MIN(axSame) AS axSame_min, MAX(axSame) AS axSame_max,
        MIN(axTransitiveProperty) AS axTransitiveProperty_min, MAX(axTransitiveProperty) AS axTransitiveProperty_max,
        MIN(axSymmetricProperty) AS axSymmetricProperty_min, MAX(axSymmetricProperty) AS axSymmetricProperty_max,
        MIN(axFunctionalDProperty) AS axFunctionalDProperty_min, MAX(axFunctionalDProperty) AS axFunctionalDProperty_max,
        MIN(axFunctionalOProperty) AS axFunctionalOProperty_min, MAX(axFunctionalOProperty) AS axFunctionalOProperty_max,
        MIN(axInverseOProperties) AS axInverseOProperties_min, MAX(axInverseOProperties) AS axInverseOProperties_max,
        MIN(axInverseFunctionalOProperties) AS axInverseFunctionalOProperties_min, MAX(axInverseFunctionalOProperties) AS axInverseFunctionalOProperties_max,
        MIN(axEquivalentDataProperties) AS axEquivalentDataProperties_min, MAX(axEquivalentDataProperties) AS axEquivalentDataProperties_max,
        MIN(axEquivalentObjectProperties) AS axEquivalentObjectProperties_min, MAX(axEquivalentObjectProperties) AS axEquivalentObjectProperties_max,
        MIN(axDataPropertyDomain) AS axDataPropertyDomain_min, MAX(axDataPropertyDomain) AS axDataPropertyDomain_max,
        MIN(axDataPropertyRange) AS axDataPropertyRange_min, MAX(axDataPropertyRange) AS axDataPropertyRange_max,
        MIN(axObjectPropertyDomain) AS axObjectPropertyDomain_min, MAX(axObjectPropertyDomain) AS axObjectPropertyDomain_max,
        MIN(axObjectPropertyRange) AS axObjectPropertyRange_min, MAX(axObjectPropertyRange) AS axObjectPropertyRange_max,
        MIN(axDataPropertyAssertion) AS axDataPropertyAssertion_min, MAX(axDataPropertyAssertion) AS axDataPropertyAssertion_max,
        MIN(axObjectPropertyAssertion) AS axObjectPropertyAssertion_min, MAX(axObjectPropertyAssertion) AS axObjectPropertyAssertion_max,
        MIN(axSubDataProperty) AS axSubDataProperty_min, MAX(axSubDataProperty) AS axSubDataProperty_max,
        MIN(axSubObjectProperty) AS axSubObjectProperty_min, MAX(axSubObjectProperty) AS axSubObjectProperty_max,
        MIN(axSubClassOf) AS axSubClassOf_min, MAX(axSubClassOf) AS axSubClassOf_max,
        MIN(constructIntersection) AS constructIntersection_min, MAX(constructIntersection) AS constructIntersection_max,
        MIN(constructUnion) AS constructUnion_min, MAX(constructUnion) AS constructUnion_max,
        MIN(constructComplement) AS constructComplement_min, MAX(constructComplement) AS constructComplement_max,
        MIN(constructSome) AS constructSome_min, MAX(constructSome) AS constructSome_max,
        MIN(constructOnly) AS constructOnly_min, MAX(constructOnly) AS constructOnly_max,
        MIN(constructHasValue) AS constructHasValue_min, MAX(constructHasValue) AS constructHasValue_max,
        MIN(constructMin) AS constructMin_min, MAX(constructMin) AS constructMin_max,
        MIN(constructExact) AS constructExact_min, MAX(constructExact) AS constructExact_max,
        MIN(constructMax) AS constructMax_min, MAX(constructMax) AS constructMax_max,
        MIN(constructHasSelf) AS constructHasSelf_min, MAX(constructHasSelf) AS constructHasSelf_max,
        MIN(constructOneOf) AS constructOneOf_min, MAX(constructOneOf) AS constructOneOf_max,
        MIN(top_classes) AS top_classes_min, MAX(top_classes) AS top_classes_max,
        MIN(leaf_classes) AS leaf_classes_min, MAX(leaf_classes) AS leaf_classes_max,
        MIN(maxDepth) AS maxDepth_min, MAX(maxDepth) AS maxDepth_max,
        MIN(avgDepth) AS avgDepth_min, MAX(avgDepth) AS avgDepth_max,
        MIN(maxBreadth) AS maxBreadth_min, MAX(maxBreadth) AS maxBreadth_max,
        MIN(avgBreadth) AS avgBreadth_min, MAX(avgBreadth) AS avgBreadth_max,
        MIN(multipleInheritance) AS multipleInheritance_min, MAX(multipleInheritance) AS multipleInheritance_max,
        MIN(avgSuperClasses) AS avgSuperClasses_min, MAX(avgSuperClasses) AS avgSuperClasses_max,
        MIN(superclasses) AS superclasses_min, MAX(superclasses) AS superclasses_max,
        MIN(numberUnsatClasses) AS numberUnsatClasses_min, MAX(numberUnsatClasses) AS numberUnsatClasses_max,
        MIN(axAnnotationAssertion) AS axAnnotationAssertion_min, MAX(axAnnotationAssertion) AS axAnnotationAssertion_max,
        MIN(labels) AS labels_min, MAX(labels) AS labels_max,
        MIN(comments) AS comments_min, MAX(comments) AS comments_max,
        MIN(seeAlso) AS seeAlso_min, MAX(seeAlso) AS seeAlso_max,
        MIN(isDefinedBy) AS isDefinedBy_min, MAX(isDefinedBy) AS isDefinedBy_max,
        MIN(multilinguality) AS multilinguality_min, MAX(multilinguality) AS multilinguality_max,
        MIN(domainAnonymous) AS domainAnonymous_min, MAX(domainAnonymous) AS domainAnonymous_max,
        MIN(domainClass) AS domainClass_min, MAX(domainClass) AS domainClass_max,
        MIN(maxClassInHierarchy) AS maxClassInHierarchy_min, MAX(maxClassInHierarchy) AS maxClassInHierarchy_max,
        MIN(avgClassInHierarchy) AS avgClassInHierarchy_min, MAX(avgClassInHierarchy) AS avgClassInHierarchy_max,
        MIN(rangeAnonymous) AS rangeAnonymous_min, MAX(rangeAnonymous) AS rangeAnonymous_max,
        MIN(rangeClass) AS rangeClass_min, MAX(rangeClass) AS rangeClass_max,
        MIN(maxRangeClassInHierarchy) AS maxRangeClassInHierarchy_min, MAX(maxRangeClassInHierarchy) AS maxRangeClassInHierarchy_max,
        MIN(avgRangeClassInHierarchy) AS avgRangeClassInHierarchy_min, MAX(avgRangeClassInHierarchy) AS avgRangeClassInHierarchy_max,
        MIN(classesMaxReusedDomainRange) AS classesMaxReusedDomainRange_min, MAX(classesMaxReusedDomainRange) AS classesMaxReusedDomainRange_max,
        MIN(classesMoreThanOnceDomainRange) AS classesMoreThanOnceDomainRange_min, MAX(classesMoreThanOnceDomainRange) AS classesMoreThanOnceDomainRange_max,
        MIN(classesExensional) AS classesExensional_min, MAX(classesExensional) AS classesExensional_max,
        MIN(maxInstancesInClassesExtensional) AS maxInstancesInClassesExtensional_min, MAX(maxInstancesInClassesExtensional) ASmaxInstancesInClassesExtensional_max,
        MIN(avgInstancesInClassesExtensional) AS avgInstancesInClassesExtensional_min, MAX(avgInstancesInClassesExtensional) AS avgInstancesInClassesExtensional_max
        FROM ontologies;`,
    {
      plain: true,
      raw: true,
      type: QueryTypes.SELECT,
    }
  );
};

module.exports = getRanges;
