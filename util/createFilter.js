/**
 * Ontology filters
 *
 * @module util
 */
const { Op } = require("sequelize");

const getRanges = require("./getRanges");

/**
 * @namespace filters
 */

/**
 * Creates query from selected filters according to which ontologies will be found
 *
 * @function
 * @name createFilter
 * @memberof module:util~filters
 * @inner
 * @async
 */
createFilter = async (body) => {
  let filter = {};
  const ranges = await getRanges();

  /**
   * Entity
   */
  if (body.selectedResource && body.selectedResource !== "all") {
    filter.resource = body.selectedResource;
  }

  if (body.classes_min || body.classes_max) {
    filter.classes = {
      [Op.between]: [
        body.classes_min || ranges.classes_min,
        body.classes_max || ranges.classes_max,
      ],
    };
  }

  if (body.classesAPIimport_min || body.classesAPIimport_max) {
    filter.classesAPIimport = {
      [Op.between]: [
        body.classesAPIimport_min || ranges.classesAPIimport_min,
        body.classesAPIimport_max || ranges.classesAPIimport_max,
      ],
    };
  }

  if (body.object_properties_min || body.object_properties_max) {
    filter.object_properties = {
      [Op.between]: [
        body.object_properties_min || ranges.object_properties_min,
        body.object_properties_max || ranges.object_properties_max,
      ],
    };
  }

  if (
    body.object_propertiesAPIimport_min ||
    body.object_propertiesAPIimport_max
  ) {
    filter.object_propertiesAPIimport = {
      [Op.between]: [
        body.object_propertiesAPIimport_min ||
          ranges.object_propertiesAPIimport_min,
        body.object_propertiesAPIimport_max ||
          ranges.object_propertiesAPIimport_max,
      ],
    };
  }

  if (body.datatype_properties_min || body.datatype_properties_max) {
    filter.datatype_properties = {
      [Op.between]: [
        body.datatype_properties_min || ranges.datatype_properties_min,
        body.datatype_properties_max || ranges.datatype_properties_max,
      ],
    };
  }

  if (
    body.datatype_propertiesAPIimport_min ||
    body.datatype_propertiesAPIimport_max
  ) {
    filter.datatype_propertiesAPIimport = {
      [Op.between]: [
        body.datatype_propertiesAPIimport_min ||
          ranges.datatype_propertiesAPIimport_min,
        body.datatype_propertiesAPIimport_max ||
          ranges.datatype_propertiesAPIimport_max,
      ],
    };
  }

  if (body.instances_min || body.instances_max) {
    filter.instances = {
      [Op.between]: [
        body.instances_min || ranges.instances_min,
        body.instances_max || ranges.instances_max,
      ],
    };
  }

  if (body.instancesAPIimport_min || body.instancesAPIimport_max) {
    filter.instancesAPIimport = {
      [Op.between]: [
        body.instancesAPIimport_min || ranges.instancesAPIimport_min,
        body.instancesAPIimport_max || ranges.instancesAPIimport_max,
      ],
    };
  }

  if (body.imports_min || body.imports_max) {
    filter.imports = {
      [Op.between]: [
        body.imports_min || ranges.imports_min,
        body.imports_max || ranges.imports_max,
      ],
    };
  }

  /**
   * Axiom
   */
  if (body.axiomCount_min || body.axiomCount_max) {
    filter.axiomCount = {
      [Op.between]: [
        body.axiomCount_min || ranges.axiomCount_min,
        body.axiomCount_max || ranges.axiomCount_max,
      ],
    };
  }

  if (body.logicalAxioms_min || body.logicalAxioms_max) {
    filter.logicalAxioms = {
      [Op.between]: [
        body.logicalAxioms_min || ranges.logicalAxioms_min,
        body.logicalAxioms_max || ranges.logicalAxioms_max,
      ],
    };
  }

  if (body.GCICount_min || body.GCICount_max) {
    filter.GCICount = {
      [Op.between]: [
        body.GCICount_min || ranges.GCICount_min,
        body.GCICount_max || ranges.GCICount_max,
      ],
    };
  }

  if (body.hiddenGCICount_min || body.hiddenGCICount_max) {
    filter.hiddenGCICount = {
      [Op.between]: [
        body.hiddenGCICount_min || ranges.hiddenGCICount_min,
        body.hiddenGCICount_max || ranges.hiddenGCICount_max,
      ],
    };
  }

  if (body.axEquivalentClasses_min || body.axEquivalentClasses_max) {
    filter.axEquivalentClasses = {
      [Op.between]: [
        body.axEquivalentClasses_min || ranges.axEquivalentClasses_min,
        body.axEquivalentClasses_max || ranges.axEquivalentClasses_max,
      ],
    };
  }

  if (body.axDisjoint_min || body.axDisjoint_max) {
    filter.axDisjoint = {
      [Op.between]: [
        body.axDisjoint_min || ranges.axDisjoint_min,
        body.axDisjoint_max || ranges.axDisjoint_max,
      ],
    };
  }

  if (body.axDisjointUnion_min || body.axDisjointUnion_max) {
    filter.axDisjointUnion = {
      [Op.between]: [
        body.axDisjointUnion_min || ranges.axDisjointUnion_min,
        body.axDisjointUnion_max || ranges.axDisjointUnion_max,
      ],
    };
  }

  if (body.axClassAssertion_min || body.axClassAssertion_max) {
    filter.axClassAssertion = {
      [Op.between]: [
        body.axClassAssertion_min || ranges.axClassAssertion_min,
        body.axClassAssertion_max || ranges.axClassAssertion_max,
      ],
    };
  }

  if (body.axDifferent_min || body.axDifferent_max) {
    filter.axDifferent = {
      [Op.between]: [
        body.axDifferent_min || ranges.axDifferent_min,
        body.axDifferent_max || ranges.axDifferent_max,
      ],
    };
  }

  if (body.axSame_min || body.axSame_max) {
    filter.axSame = {
      [Op.between]: [
        body.axSame_min || ranges.axSame_min,
        body.axSame_max || ranges.axSame_max,
      ],
    };
  }

  if (body.axTransitiveProperty_min || body.axTransitiveProperty_max) {
    filter.axTransitiveProperty = {
      [Op.between]: [
        body.axTransitiveProperty_min || ranges.axTransitiveProperty_min,
        body.axTransitiveProperty_max || ranges.axTransitiveProperty_max,
      ],
    };
  }

  if (body.axSymmetricProperty_min || body.axSymmetricProperty_max) {
    filter.axSymmetricProperty = {
      [Op.between]: [
        body.axSymmetricProperty_min || ranges.axSymmetricProperty_min,
        body.axSymmetricProperty_max || ranges.axSymmetricProperty_max,
      ],
    };
  }

  if (body.axFunctionalDProperty_min || body.axFunctionalDProperty_max) {
    filter.axFunctionalDProperty = {
      [Op.between]: [
        body.axFunctionalDProperty_min || ranges.axFunctionalDProperty_min,
        body.axFunctionalDProperty_max || ranges.axFunctionalDProperty_max,
      ],
    };
  }

  if (body.axFunctionalOProperty_min || body.axFunctionalOProperty_max) {
    filter.axFunctionalOProperty = {
      [Op.between]: [
        body.axFunctionalOProperty_min || ranges.axFunctionalOProperty_min,
        body.axFunctionalOProperty_max || ranges.axFunctionalOProperty_max,
      ],
    };
  }

  if (body.axInverseOProperties_min || body.axInverseOProperties_max) {
    filter.axInverseOProperties = {
      [Op.between]: [
        body.axInverseOProperties_min || ranges.axInverseOProperties_min,
        body.axInverseOProperties_max || ranges.axInverseOProperties_max,
      ],
    };
  }

  if (
    body.axInverseFunctionalOProperties_min ||
    body.axInverseFunctionalOProperties_max
  ) {
    filter.axInverseFunctionalOProperties = {
      [Op.between]: [
        body.axInverseFunctionalOProperties_min ||
          ranges.axInverseFunctionalOProperties_min,
        body.axInverseFunctionalOProperties_max ||
          ranges.axInverseFunctionalOProperties_max,
      ],
    };
  }

  if (
    body.axEquivalentDataProperties_min ||
    body.axEquivalentDataProperties_max
  ) {
    filter.axEquivalentDataProperties = {
      [Op.between]: [
        body.axEquivalentDataProperties_min ||
          ranges.axEquivalentDataProperties_min,
        body.axEquivalentDataProperties_max ||
          ranges.axEquivalentDataProperties_max,
      ],
    };
  }

  if (
    body.axEquivalentObjectProperties_min ||
    body.axEquivalentObjectProperties_max
  ) {
    filter.axEquivalentObjectProperties = {
      [Op.between]: [
        body.axEquivalentObjectProperties_min ||
          ranges.axEquivalentObjectProperties_min,
        body.axEquivalentObjectProperties_max ||
          ranges.axEquivalentObjectProperties_max,
      ],
    };
  }

  if (body.axDataPropertyDomain_min || body.axDataPropertyDomain_max) {
    filter.axDataPropertyDomain = {
      [Op.between]: [
        body.axDataPropertyDomain_min || ranges.axDataPropertyDomain_min,
        body.axDataPropertyDomain_max || ranges.axDataPropertyDomain_max,
      ],
    };
  }

  if (body.axDataPropertyRange_min || body.axDataPropertyRange_max) {
    filter.axDataPropertyRange = {
      [Op.between]: [
        body.axDataPropertyRange_min || ranges.axDataPropertyRange_min,
        body.axDataPropertyRange_max || ranges.axDataPropertyRange_max,
      ],
    };
  }

  if (body.axObjectPropertyDomain_min || body.axObjectPropertyDomain_max) {
    filter.axObjectPropertyDomain = {
      [Op.between]: [
        body.axObjectPropertyDomain_min || ranges.axObjectPropertyDomain_min,
        body.axObjectPropertyDomain_max || ranges.axObjectPropertyDomain_max,
      ],
    };
  }

  if (body.axObjectPropertyRange_min || body.axObjectPropertyRange_max) {
    filter.axObjectPropertyRange = {
      [Op.between]: [
        body.axObjectPropertyRange_min || ranges.axObjectPropertyRange_min,
        body.axObjectPropertyRange_max || ranges.axObjectPropertyRange_max,
      ],
    };
  }

  if (body.axDataPropertyAssertion_min || body.axDataPropertyAssertion_max) {
    filter.axDataPropertyAssertion = {
      [Op.between]: [
        body.axDataPropertyAssertion_min || ranges.axDataPropertyAssertion_min,
        body.axDataPropertyAssertion_max || ranges.axDataPropertyAssertion_max,
      ],
    };
  }

  if (
    body.axObjectPropertyAssertion_min ||
    body.axObjectPropertyAssertion_max
  ) {
    filter.axObjectPropertyAssertion = {
      [Op.between]: [
        body.axObjectPropertyAssertion_min ||
          ranges.axObjectPropertyAssertion_min,
        body.axObjectPropertyAssertion_max ||
          ranges.axObjectPropertyAssertion_max,
      ],
    };
  }

  if (body.axSubDataProperty_min || body.axSubDataProperty_max) {
    filter.axSubDataProperty = {
      [Op.between]: [
        body.axSubDataProperty_min || ranges.axSubDataProperty_min,
        body.axSubDataProperty_max || ranges.axSubDataProperty_max,
      ],
    };
  }

  if (body.axSubObjectProperty_min || body.axSubObjectProperty_max) {
    filter.axSubObjectProperty = {
      [Op.between]: [
        body.axSubObjectProperty_min || ranges.axSubObjectProperty_min,
        body.axSubObjectProperty_max || ranges.axSubObjectProperty_max,
      ],
    };
  }

  if (body.axSubClassOf_min || body.axSubClassOf_max) {
    filter.axSubClassOf = {
      [Op.between]: [
        body.axSubClassOf_min || ranges.axSubClassOf_min,
        body.axSubClassOf_max || ranges.axSubClassOf_max,
      ],
    };
  }

  /**
   * Class expression type
   */
  if (body.constructIntersection_min || body.constructIntersection_max) {
    filter.constructIntersection = {
      [Op.between]: [
        body.constructIntersection_min || ranges.constructIntersection_min,
        body.constructIntersection_max || ranges.constructIntersection_max,
      ],
    };
  }

  if (body.constructUnion_min || body.constructUnion_max) {
    filter.constructUnion = {
      [Op.between]: [
        body.constructUnion_min || ranges.constructUnion_min,
        body.constructUnion_max || ranges.constructUnion_max,
      ],
    };
  }

  if (body.constructComplement_min || body.constructComplement_max) {
    filter.constructComplement = {
      [Op.between]: [
        body.constructComplement_min || ranges.constructComplement_min,
        body.constructComplement_max || ranges.constructComplement_max,
      ],
    };
  }

  if (body.constructSome_min || body.constructSome_max) {
    filter.constructSome = {
      [Op.between]: [
        body.constructSome_min || ranges.constructSome_min,
        body.constructSome_max || ranges.constructSome_max,
      ],
    };
  }

  if (body.constructOnly_min || body.constructOnly_max) {
    filter.constructOnly = {
      [Op.between]: [
        body.constructOnly_min || ranges.constructOnly_min,
        body.constructOnly_max || ranges.constructOnly_max,
      ],
    };
  }

  if (body.constructHasValue_min || body.constructHasValue_max) {
    filter.constructHasValue = {
      [Op.between]: [
        body.constructHasValue_min || ranges.constructHasValue_min,
        body.constructHasValue_max || ranges.constructHasValue_max,
      ],
    };
  }

  if (body.constructMin_min || body.constructMin_max) {
    filter.constructMin = {
      [Op.between]: [
        body.constructMin_min || ranges.constructMin_min,
        body.constructMin_max || ranges.constructMin_max,
      ],
    };
  }

  if (body.constructExact_min || body.constructExact_max) {
    filter.constructExact = {
      [Op.between]: [
        body.constructExact_min || ranges.constructExact_min,
        body.constructExact_max || ranges.constructExact_max,
      ],
    };
  }

  if (body.constructMax_min || body.constructMax_max) {
    filter.constructMax = {
      [Op.between]: [
        body.constructMax_min || ranges.constructMax_min,
        body.constructMax_max || ranges.constructMax_max,
      ],
    };
  }

  if (body.constructHasSelf_min || body.constructHasSelf_max) {
    filter.constructHasSelf = {
      [Op.between]: [
        body.constructHasSelf_min || ranges.constructHasSelf_min,
        body.constructHasSelf_max || ranges.constructHasSelf_max,
      ],
    };
  }

  if (body.constructOneOf_min || body.constructOneOf_max) {
    filter.constructOneOf = {
      [Op.between]: [
        body.constructOneOf_min || ranges.constructOneOf_min,
        body.constructOneOf_max || ranges.constructOneOf_max,
      ],
    };
  }

  /**
   * Taxonomy
   */
  if (body.top_classes_min || body.top_classes_max) {
    filter.top_classes = {
      [Op.between]: [
        body.top_classes_min || ranges.top_classes_min,
        body.top_classes_max || ranges.top_classes_max,
      ],
    };
  }

  if (body.leaf_classes_min || body.leaf_classes_max) {
    filter.leaf_classes = {
      [Op.between]: [
        body.leaf_classes_min || ranges.leaf_classes_min,
        body.leaf_classes_max || ranges.leaf_classes_max,
      ],
    };
  }

  if (body.maxDepth_min || body.maxDepth_max) {
    filter.maxDepth = {
      [Op.between]: [
        body.maxDepth_min || ranges.maxDepth_min,
        body.maxDepth_max || ranges.maxDepth_max,
      ],
    };
  }

  if (body.avgDepth_min || body.avgDepth_max) {
    filter.avgDepth = {
      [Op.between]: [
        body.avgDepth_min || ranges.avgDepth_min,
        body.avgDepth_max || ranges.avgDepth_max,
      ],
    };
  }

  if (body.maxBreadth_min || body.maxBreadth_max) {
    filter.maxBreadth = {
      [Op.between]: [
        body.maxBreadth_min || ranges.maxBreadth_min,
        body.maxBreadth_max || ranges.maxBreadth_max,
      ],
    };
  }

  if (body.avgBreadth_min || body.avgBreadth_max) {
    filter.avgBreadth = {
      [Op.between]: [
        body.avgBreadth_min || ranges.avgBreadth_min,
        body.avgBreadth_max || ranges.avgBreadth_max,
      ],
    };
  }

  if (body.multipleInheritance_min || body.multipleInheritance_max) {
    filter.multipleInheritance = {
      [Op.between]: [
        body.multipleInheritance_min || ranges.multipleInheritance_min,
        body.multipleInheritance_max || ranges.multipleInheritance_max,
      ],
    };
  }

  if (body.avgSuperClasses_min || body.avgSuperClasses_max) {
    filter.avgSuperClasses = {
      [Op.between]: [
        body.avgSuperClasses_min || ranges.avgSuperClasses_min,
        body.avgSuperClasses_max || ranges.avgSuperClasses_max,
      ],
    };
  }

  if (body.superclasses_min || body.superclasses_max) {
    filter.superclasses = {
      [Op.between]: [
        body.superclasses_min || ranges.superclasses_min,
        body.superclasses_max || ranges.superclasses_max,
      ],
    };
  }

  /**
   * OWL2 profiles and reasoning
   */
  if (body.OWL2) {
    filter.OWL2 = body.OWL2;
  }

  if (body.OWL2DL) {
    filter.OWL2DL = body.OWL2DL;
  }

  if (body.OWL2EL) {
    filter.OWL2EL = body.OWL2EL;
  }

  if (body.OWL2QL) {
    filter.OWL2QL = body.OWL2QL;
  }

  if (body.OWL2RL) {
    filter.OWL2RL = body.OWL2RL;
  }

  if (body.consistent) {
    filter.consistent = body.consistent;
  }

  if (body.numberUnsatClasses_min || body.numberUnsatClasses_max) {
    filter.numberUnsatClasses = {
      [Op.between]: [
        body.numberUnsatClasses_min || ranges.numberUnsatClasses_min,
        body.numberUnsatClasses_max || ranges.numberUnsatClasses_max,
      ],
    };
  }

  /**
   * Annotation
   */
  if (body.axAnnotationAssertion_min || body.axAnnotationAssertion_max) {
    filter.axAnnotationAssertion = {
      [Op.between]: [
        body.axAnnotationAssertion_min || ranges.axAnnotationAssertion_min,
        body.axAnnotationAssertion_max || ranges.axAnnotationAssertion_max,
      ],
    };
  }

  if (body.labels_min || body.labels_max) {
    filter.labels = {
      [Op.between]: [
        body.labels_min || ranges.labels_min,
        body.labels_max || ranges.labels_max,
      ],
    };
  }

  if (body.comments_min || body.comments_max) {
    filter.comments = {
      [Op.between]: [
        body.comments_min || ranges.comments_min,
        body.comments_max || ranges.comments_max,
      ],
    };
  }

  if (body.seeAlso_min || body.seeAlso_max) {
    filter.seeAlso = {
      [Op.between]: [
        body.seeAlso_min || ranges.seeAlso_min,
        body.seeAlso_max || ranges.seeAlso_max,
      ],
    };
  }

  if (body.isDefinedBy_min || body.isDefinedBy_max) {
    filter.isDefinedBy = {
      [Op.between]: [
        body.isDefinedBy_min || ranges.isDefinedBy_min,
        body.isDefinedBy_max || ranges.isDefinedBy_max,
      ],
    };
  }

  if (body.multilinguality_min || body.multilinguality_max) {
    filter.multilinguality = {
      [Op.between]: [
        body.multilinguality_min || ranges.multilinguality_min,
        body.multilinguality_max || ranges.multilinguality_max,
      ],
    };
  }

  /**
   * Detail
   */
  if (body.domainAnonymous_min || body.domainAnonymous_max) {
    filter.domainAnonymous = {
      [Op.between]: [
        body.domainAnonymous_min || ranges.domainAnonymous_min,
        body.domainAnonymous_max || ranges.domainAnonymous_max,
      ],
    };
  }

  if (body.domainClass_min || body.domainClass_max) {
    filter.domainClass = {
      [Op.between]: [
        body.domainClass_min || ranges.domainClass_min,
        body.domainClass_max || ranges.domainClass_max,
      ],
    };
  }

  if (body.maxClassInHierarchy_min || body.maxClassInHierarchy_max) {
    filter.maxClassInHierarchy = {
      [Op.between]: [
        body.maxClassInHierarchy_min || ranges.maxClassInHierarchy_min,
        body.maxClassInHierarchy_max || ranges.maxClassInHierarchy_max,
      ],
    };
  }

  if (body.avgClassInHierarchy_min || body.avgClassInHierarchy_max) {
    filter.avgClassInHierarchy = {
      [Op.between]: [
        body.avgClassInHierarchy_min || ranges.avgClassInHierarchy_min,
        body.avgClassInHierarchy_max || ranges.avgClassInHierarchy_max,
      ],
    };
  }

  if (body.rangeAnonymous_min || body.rangeAnonymous_max) {
    filter.rangeAnonymous = {
      [Op.between]: [
        body.rangeAnonymous_min || ranges.rangeAnonymous_min,
        body.rangeAnonymous_max || ranges.rangeAnonymous_max,
      ],
    };
  }

  if (body.rangeClass_min || body.rangeClass_max) {
    filter.rangeClass = {
      [Op.between]: [
        body.rangeClass_min || ranges.rangeClass_min,
        body.rangeClass_max || ranges.rangeClass_max,
      ],
    };
  }

  if (body.maxRangeClassInHierarchy_min || body.maxRangeClassInHierarchy_max) {
    filter.maxRangeClassInHierarchy = {
      [Op.between]: [
        body.maxRangeClassInHierarchy_min ||
          ranges.maxRangeClassInHierarchy_min,
        body.maxRangeClassInHierarchy_max ||
          ranges.maxRangeClassInHierarchy_max,
      ],
    };
  }

  if (body.avgRangeClassInHierarchy_min || body.avgRangeClassInHierarchy_max) {
    filter.avgRangeClassInHierarchy = {
      [Op.between]: [
        body.avgRangeClassInHierarchy_min ||
          ranges.avgRangeClassInHierarchy_min,
        body.avgRangeClassInHierarchy_max ||
          ranges.avgRangeClassInHierarchy_max,
      ],
    };
  }

  if (
    body.classesMaxReusedDomainRange_min ||
    body.classesMaxReusedDomainRange_max
  ) {
    filter.classesMaxReusedDomainRange = {
      [Op.between]: [
        body.classesMaxReusedDomainRange_min ||
          ranges.classesMaxReusedDomainRange_min,
        body.classesMaxReusedDomainRange_max ||
          ranges.classesMaxReusedDomainRange_max,
      ],
    };
  }

  if (
    body.classesMoreThanOnceDomainRange_min ||
    body.classesMoreThanOnceDomainRange_max
  ) {
    filter.classesMoreThanOnceDomainRange = {
      [Op.between]: [
        body.classesMoreThanOnceDomainRange_min ||
          ranges.classesMoreThanOnceDomainRange_min,
        body.classesMoreThanOnceDomainRange_max ||
          ranges.classesMoreThanOnceDomainRange_max,
      ],
    };
  }

  if (body.classesExensional_min || body.classesExensional_max) {
    filter.classesExensional = {
      [Op.between]: [
        body.classesExensional_min || ranges.classesExensional_min,
        body.classesExensional_max || ranges.classesExensional_max,
      ],
    };
  }

  if (
    body.maxInstancesInClassesExtensional_min ||
    body.maxInstancesInClassesExtensional_max
  ) {
    filter.maxInstancesInClassesExtensional = {
      [Op.between]: [
        body.maxInstancesInClassesExtensional_min ||
          ranges.maxInstancesInClassesExtensional_min,
        body.maxInstancesInClassesExtensional_max ||
          ranges.maxInstancesInClassesExtensional_max,
      ],
    };
  }

  if (
    body.avgInstancesInClassesExtensional_min ||
    body.avgInstancesInClassesExtensional_max
  ) {
    filter.avgInstancesInClassesExtensional = {
      [Op.between]: [
        body.avgInstancesInClassesExtensional_min ||
          ranges.avgInstancesInClassesExtensional_min,
        body.avgInstancesInClassesExtensional_max ||
          ranges.avgInstancesInClassesExtensional_max,
      ],
    };
  }

  return filter;
};

module.exports = createFilter;
