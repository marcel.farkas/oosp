import React, { Component } from "react";
import { BsDownload } from "react-icons/bs";
import { GrDocumentZip } from "react-icons/gr";
import { BiShow } from "react-icons/bi";
import Button from "react-bootstrap/Button";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { ColumnToggle } from "react-bootstrap-table2-toolkit";
import { Accordion, Card } from "react-bootstrap";
import paginationFactory from "react-bootstrap-table2-paginator";
import Spinner from "react-bootstrap/Spinner";
import Modal from "react-bootstrap/Modal";
import ChartPopup from "./ChartPopup";
import ClassesFrequencyDistribution from "./Charts/ClassesFrequencyDistribution";
import InstancesFrequencyDistribution from "./Charts/InstancesFrequencyDistribution";
import AxiomFrequencyDistribution from "./Charts/AxiomFrequencyDistribution";
import AxiomTypesDistribution from "./Charts/AxiomTypesDistribution";
import DLConstructsDistribution from "./Charts/DLConstructsDistribution";
import OWL2ProfilesDistribution from "./Charts/OWL2ProfilesDistribution";
import AnnotationsDistribution from "./Charts/AnnotationsDistribution";
import DomainRangeDefinitionDistribution from "./Charts/DomainRangeDefinitionDistribution";
import { min, median, mean, std, max } from "mathjs";

/**
 * Ontologies table component which displays table of result ontologies
 *
 * @component
 */
class OntologiesTableClass extends Component {
  /** @constructor */
  constructor(props) {
    super(props);
    this.state = {
      isZipping: false,
      showPopup: false,
      popupMessage: "",
      popupTitle: "",
      randomOntologiesNumber: "",
    };
  }

  /**
   * Smooth scroll to the table with result ontologies
   *
   * @method
   */
  scrollToTable = () =>
    this.props.tableRef.current.scrollIntoView({ behavior: "smooth" });

  /**
   * Hides popups on close press
   *
   * @method
   */
  hidePopup = () => {
    this.setState({ showPopup: false });
  };

  /**
   * Saves the number of required ontologies for random subsampling
   *
   * @param {Object} evt - Input element onChange event
   * @method
   */
  randomOntologiesNumberChange = (evt) => {
    this.setState({ randomOntologiesNumber: evt.target.value });
  };

  /**
   * Selects randomly ontologies from actual selection based on entered number
   *
   * @method
   */
  randomSubSampling = () => {
    const { randomOntologiesNumber } = this.state;
    const { data } = this.props;
    // const dataTemp = [...data];

    if (
      Number.isInteger(parseInt(randomOntologiesNumber)) &&
      randomOntologiesNumber > 0 &&
      randomOntologiesNumber < data.length
    ) {
      while (data.length > randomOntologiesNumber) {
        let randomNumber = Math.floor(Math.random() * data.length);
        this.setState({ data: [] });
        data.splice(randomNumber, 1);
      }
      this.setState({ data: data }, () => this.scrollToTable());
    } else {
      this.setState({
        showPopup: true,
        popupMessage: `Please enter a positive number of randomly selected ontologies lower than number of currently selected ontologies (${data.length}).`,
        popupTitle: "Wrong number",
      });
    }
  };

  /**
   * Downloads individual ontology based on storage code
   *
   * @param {Number} code - Storage code of selected ontology
   * @async
   */
  async download(code) {
    const res = await fetch(`/download/${code}`, { method: "POST" });

    const blob = await res.blob();
    const url = window.URL.createObjectURL(blob);
    let link = document.createElement("a");
    link.href = url;
    link.download = `${code}.owl`;
    link.click();
  }

  /**
   * Downloads ontology with all its impots based on storage code
   *
   * @param {Number} code - Storage code of selected ontology
   * @async
   */
  async downloadWithImports(code) {
    const res = await fetch(`/downloadWithImports/${code}`, { method: "POST" });

    const blob = await res.blob();
    const url = window.URL.createObjectURL(blob);
    let link = document.createElement("a");
    link.href = url;
    link.download = `${code}_with_imports.zip`;
    link.click();
  }

  /**
   * Downloads ontology metrics table as a CSV file
   *
   * @method
   */
  downloadMetrics = () => {
    const { data } = this.props;
    const csvString = [
      Object.keys(data[0]),
      ...data.map((item) => {
        return Object.keys(data[0]).map((key) => {
          return item[key];
        });
      }),
    ]
      .map((e) => e.join(","))
      .join("\r\n");
    const blob = new Blob([csvString], { type: "text/csv" });
    const url = window.URL.createObjectURL(blob);
    const link = document.createElement("a");
    link.href = url;
    link.download = `metrics_table.csv`;
    document.body.appendChild(link);
    link.click();
  };

  /**
   * Downloads summary statistics as a CSV file
   *
   * @method
   */
  downloadSummaryStatistics = () => {
    const {
      resourceFilter,
      data,
      filters,
      page,
      word,
      matcher,
      entities,
      scope,
    } = this.props;
    let metricsSelection = "";
    let resource = "";

    if (page === "1") {
      resource = filters.selectedResource;
      metricsSelection = Object.keys(filters)
        .filter((item) => item !== "selectedResource" && filters[item])
        .map((item) => {
          return `${item}: ${filters[item]}`;
        })
        .join(" and ");
    }

    if (page === "2") {
      resource = resourceFilter;
      metricsSelection = `Word: ${word} and Matcher: ${matcher} and Entities: ${entities} and Scope: ${scope}`;
    }

    const metrics = Object.keys(data[0])
      .filter((item) => {
        return (
          item !== "storage_code" &&
          item !== "URL" &&
          item !== "IRI" &&
          item !== "resource" &&
          item !== "complexity" &&
          item !== "classesExensional" &&
          item !== "maxInstancesInClassesExtensional" &&
          item !== "avgInstancesInClassesExtensional" &&
          item !== "complexityBin" &&
          item !== "avgSubClasses" &&
          item !== "GCICount" &&
          item !== "subclasses" &&
          item !== "constructHasSelf" &&
          item !== "constructOneOf" &&
          item !== "axDisjointUnion" &&
          item !== "axInverseFunctionalOProperties" &&
          item !== "GCICountAPIimport" &&
          item !== "axDisjointUnionAPIimport" &&
          item !== "v5" &&
          item !== "v5avg" &&
          item !== "fc5" &&
          item !== "fc5ratio"
        );
      })
      .map((metric) => {
        return [
          metric,
          data.filter((item) => item[metric] > 0).length,
          min(data.map((item) => item[metric])),
          median(data.map((item) => item[metric])).toFixed(1),
          mean(data.map((item) => item[metric])).toFixed(1),
          std(data.map((item) => item[metric])).toFixed(1),
          max(data.map((item) => item[metric])),
        ];
      });

    const csvString = [
      ["#metrics", "nonzero", "min", "median", "average", "stdev", "max"],
      [`#${data.length} ontologies in collection`],
      [`#selected ontology pool: ${resource}`],
      [`#your metrics selection: ${metricsSelection}`],
      ...metrics,
    ]
      .map((e) => e.join(","))
      .join("\r\n");

    const blob = new Blob([csvString], { type: "text/csv" });
    const url = window.URL.createObjectURL(blob);
    const link = document.createElement("a");
    link.href = url;
    link.download = `summary_statistics.csv`;
    document.body.appendChild(link);
    link.click();
  };

  /**
   * Downloads all currently selected ontologies
   *
   * @method
   */
  downloadAllOntologies = async () => {
    const {
      data,
      filters,
      page,
      word,
      matcher,
      entities,
      scope,
      resourceFilter,
    } = this.props;

    if (data.length) {
      this.setState({ isZipping: true });
      try {
        const res = await fetch(`/downloadAll`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Accept: "application/zip",
          },
          body: JSON.stringify({
            storage_codes: data.map((item) => item.storage_code),
            metrics: filters || {
              word,
              matcher,
              entities,
              scope,
              resourceFilter,
            },
            page: page,
          }),
        });
        if (res.status === 413) {
          const data = await res.json();
          this.setState({
            showPopup: true,
            popupMessage: data.message,
            popupTitle: "Error",
          });
        } else {
          const blob = await res.blob();
          const url = window.URL.createObjectURL(blob);
          let link = document.createElement("a");
          link.href = url;
          link.download = "ontologies.zip";
          link.click();
        }
      } catch (e) {
        console.error(e);
      }
      this.setState({ isZipping: false });
    }
  };

  /**
   * Formats first column of the ontology result table to display buttons
   *
   * @param {Object} cell - current cell
   * @param {Object} row  - current row
   * @returns formatted cells
   * @method
   */
  actionFormatter = (cell, row) => {
    return (
      <>
        <BiShow
          onClick={() =>
            window.open(
              `http://visualdataweb.de/webvowl/#iri=https://owl.vse.cz/ontologies/${row.storage_code}.owl`
            )
          }
          cursor="pointer"
          style={{ margin: "0 3px" }}
        />

        <BsDownload
          onClick={() => this.download(row.storage_code)}
          cursor="pointer"
          style={{ margin: "0 3px" }}
        />

        <GrDocumentZip
          onClick={() => this.downloadWithImports(row.storage_code)}
          cursor="pointer"
          style={{ margin: "0 3px" }}
        />
      </>
    );
  };

  /**
   * Checks if column should be initially shown or hidden
   *
   * @param {String} header - name of the column
   * @returns TRUE if should be hidden, FALSE if not
   * @method
   */
  isHidden = (header) => {
    const notHiddenColumns = ["storage_code", "IRI", "resource"];

    return !notHiddenColumns.includes(header);
  };

  /**
   * Renders ontology result table
   *
   * @method
   */
  render() {
    const { ToggleList } = ColumnToggle;
    const { data } = this.props;
    const {
      isZipping,
      showPopup,
      popupMessage,
      popupTitle,
      randomOntologiesNumber,
    } = this.state;
    const headers = data.length > 0 ? Object.keys(data[0]) : null;
    let table, buttonDownloadAll;

    if (headers) {
      const columns = headers.map((header) => {
        return {
          text: header,
          dataField: header,
          hidden: this.isHidden(header),
        };
      });
      columns.unshift({
        text: "Actions",
        dataField: "actions",
        isDummyField: true,
        formatter: this.actionFormatter,
      });

      if (isZipping) {
        buttonDownloadAll = (
          <>
            <Button variant="primary" disabled>
              <Spinner
                as="span"
                animation="border"
                size="sm"
                role="status"
                aria-hidden="true"
              />
              <span className="sr-only">Loading...</span>
            </Button>
          </>
        );
      } else {
        buttonDownloadAll = (
          <Button onClick={() => this.downloadAllOntologies()}>
            Download all ({data.length})
          </Button>
        );
      }

      table = (
        <ToolkitProvider
          keyField="storage_code"
          data={data}
          columns={columns}
          columnToggle
        >
          {(props) => (
            <div>
              <div className="random-subsampling">
                <hr />
                Number of ontologies which will be randomly selected for your
                experimental ontology set:
                <br />
                <Button onClick={this.randomSubSampling}>
                  Random sub-sampling
                </Button>
                <input
                  value={randomOntologiesNumber}
                  onChange={this.randomOntologiesNumberChange}
                />
              </div>
              <hr />
              <div className="chart-group">
                <h5>Graphs for Ontology Set Statistics</h5>
                <ChartPopup
                  chart={<ClassesFrequencyDistribution data={data} />}
                  name="Class frequency distribution"
                />
                <ChartPopup
                  chart={<InstancesFrequencyDistribution data={data} />}
                  name="Instances frequency distribution"
                />
                <ChartPopup
                  chart={<AxiomFrequencyDistribution data={data} />}
                  name="Axiom frequency distribution"
                />
                <ChartPopup
                  chart={<AxiomTypesDistribution data={data} />}
                  name="Axiom types distribution"
                />
                <ChartPopup
                  chart={<DLConstructsDistribution data={data} />}
                  name="DL constructs distribution"
                />
                <ChartPopup
                  chart={<OWL2ProfilesDistribution data={data} />}
                  name="OWL2 profiles distribution"
                />
                <ChartPopup
                  chart={<AnnotationsDistribution data={data} />}
                  name="Annotations distribution"
                />
                <ChartPopup
                  chart={<DomainRangeDefinitionDistribution data={data} />}
                  name="Domain/Range definition distribution"
                />
              </div>
              <hr />
              <p className="download-info">
                You can download all selected ontologies by clicking "Download
                all" button below, open ontology in WebVOWL by clicking{" "}
                <BiShow /> icon, download individual ontology by clicking{" "}
                <BsDownload /> icon or download ontology with its imports as a
                zip file by clicking <GrDocumentZip /> icon.
              </p>

              <div className="ontologies-table-btn-group">
                {buttonDownloadAll}
                <Button onClick={() => this.downloadSummaryStatistics()}>
                  Download summary statistics
                </Button>
                <Button onClick={() => this.downloadMetrics()}>
                  Download ontology metrics table
                </Button>
              </div>

              <Accordion
                className="ontology-table-columns-toggle-accordion"
                ref={this.props.tableRef}
              >
                <Card>
                  <Accordion.Toggle as={Card.Header} eventKey="0">
                    Show/Hide Columns
                  </Accordion.Toggle>
                  <Accordion.Collapse eventKey="0">
                    <Card.Body>
                      <ToggleList
                        {...props.columnToggleProps}
                        btnClassName="ontology-table-columns-toggle-item"
                        className="ontology-table-columns-toggle"
                      />
                    </Card.Body>
                  </Accordion.Collapse>
                </Card>
              </Accordion>

              <br />

              <BootstrapTable
                {...props.baseProps}
                pagination={paginationFactory()}
              />
            </div>
          )}
        </ToolkitProvider>
      );
    } else {
      table = "";
    }

    return (
      <div>
        <Modal show={showPopup} onHide={this.hidePopup}>
          <Modal.Header closeButton>
            <Modal.Title>{popupTitle}</Modal.Title>
          </Modal.Header>
          <Modal.Body>{popupMessage}</Modal.Body>
          {/* <Modal.Footer>
    <Button variant="secondary" onClick={handleClose}>Close</Button>
    <Button variant="primary" onClick={handleClose}>
      Save Changes
    </Button>
  </Modal.Footer> */}
        </Modal>
        {table}
      </div>
    );
  }
}

export default OntologiesTableClass;
