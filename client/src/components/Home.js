/**
 * Main page
 *
 * @component
 */
function Home() {
  return (
    <div className="home">
      <h1>OOSP: Online Ontology Set Picker</h1>
      <div className="content">
        <h4>Purpose:</h4>
        <p>
          Online Ontology Set Picker (OOSP) allows to select, from major
          repositories, ontologies that satisfy a user-defined sets of metrics.
          Its main purpose is allowing ontological tool designers to rapidly
          build custom benchmarks on which they could test different features.
          It could also serve for usage studies of different ontology language
          constructs and for pattern spotting. The web front-end allows to
          specify a broad range of metrics and delivers benchmarks along with
          their statistics of metrics, including a graph view.{" "}
        </p>
        <hr />

        <h4>Publications:</h4>
        <div>
          <p>
            Zamazal O., Svátek V.: OOSP: Ontological Benchmarks Made on the Fly.
            In: 1st International Workshop on Summarizing and Presenting
            Entities and Ontologies at ESWC 2015. Portoroz, Slovenia. 2015.{" "}
          </p>
          <p>
            Zamazal O., Svátek V.: Ontology Search by Categorization Power. In:
            2nd International Workshop on Summarizing and Presenting Entities
            and Ontologies at ESWC 2016. Anissaras, Greece. 2016.{" "}
          </p>
          <p>
            Zamazal O., Svátek V.: Facilitating Ontological Benchmark
            Construction Using Similarity Computation and Formal Concept
            Analysis. In: SEMANTiCS 2016 Posters and Demos track. Accepted.{" "}
          </p>
          <p>
            Zamazal O., Dudáš M., Svátek V.: Augmenting the Ontology
            Visualization Tool Recommender: Input Pre-Filling and Integration
            with the OOSP Ontological Benchmark Builder. In: SEMANTiCS 2016
            Posters and Demos track. Accepted.{" "}
          </p>
        </div>
        <hr />
        <h4>Ontology Repositories:</h4>
        <div>
          <p>
            Currently, it offers two well-curated repositories: BioPortal and
            Linked Open Vocabularies (LOV). Further, in 2016 we included
            OntoFarm and NanJing Vocabulary Repository. We processed them using
            OWLAPI, thus we only include ontologies parseable by OWLAPI.
            Further, we could not process those ontologies which were not
            publicly available, were not available (or one of its imported
            ontologies) from its hosting server. As a results, we have the
            following snapshots:{" "}
          </p>
          <ul>
            <li>
              BioPortal February, 2015 snapshot includes 317 out of 375
              ontologies, i.e. 85%.
            </li>
            <li>
              LOV February, 2015 snapshot includes 461 out of 475 ontologies,
              i.e. 97%.
            </li>
            <li>
              OntoFarm January, 2016 snapshot includes 16 ontologies, i.e. 100%.
            </li>
            <li>
              LOV January, 2016 snapshot includes 509 out of 529 ontologies,
              i.e. 96%.
            </li>
            <li>
              BioPortal January, 2016 snapshot includes 399 out of 501
              ontologies, i.e. 80%.
            </li>
            <li>
              NanJing Vocabulary Repository, 2016 snapshot includes 1403 out of
              1532 ontologies, i.e. 92%.
            </li>
            <li>
              NanJing Vocabulary Repository merged, 2016 snapshot includes 225
              out of 231 ontologies, i.e. 97%.
            </li>
            <li>
              LOV November, 2017 snapshot includes 568 out of 617 ontologies,
              i.e. 92%.
            </li>
            <li>
              (new) BioPortal November, 2017 snapshot includes 494 out of 657
              ontologies, i.e. 75%.
            </li>
          </ul>
          <hr />
          <h4>Ontology Metrics:</h4>
          <div>
            <p>
              We include various ontology metrics from the following 7
              categories:
            </p>
            <ul>
              <li>
                Entity metrics include numbers of entities (e.g. classes,
                instances). Newly there is added "Metrics Based on Focus
                Classes".
              </li>
              <li>
                Axiom metrics include numbers of different axiom types (e.g.
                subsumptions, equivalencies).
              </li>
              <li>
                Class expression types metrics include expressions used for
                construction of anonymous classes (e.g. existential
                quantification).
              </li>
              <li>
                Taxonomy metrics include characteristics of taxonomy (e.g. a
                number of top classes, leaf classes, branching degree or maximum
                taxonomy depth).
              </li>
              <li>
                OWL2 profiles and reasoning metrics include profile information
                along with information about consistency and a number of
                unsatisfiable classes.
              </li>
              <li>
                Annotations metrics include numbers of several selected
                annotation types (e.g. labels, comments, number of different
                languages involved in labels annotations).
              </li>
              <li>
                Detail metrics include some experimentally designed metrics
                related to domain/range (e.g. number of anonymous classes as
                domain definition).{" "}
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Home;
