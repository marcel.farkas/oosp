import React, { Component } from "react";
import PropTypes from "prop-types";

/**
 * Individual tabs component
 *
 * @component
 */
class Tab extends Component {
  static propTypes = {
    activeTab: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
  };

  /**
   * Passes name of a selected tab to parent
   *
   * @method
   */
  onClick = () => {
    const { label, onClick } = this.props;
    onClick(label);
  };

  /**
   * Renders individual tabs
   *
   * @method
   */
  render() {
    const {
      onClick,
      props: { activeTab, label },
    } = this;

    let className = "tab-list-item";

    if (activeTab === label) {
      className += " tab-list-active";
    }

    return (
      <li className={className} onClick={onClick}>
        {label}
      </li>
    );
  }
}

export default Tab;
