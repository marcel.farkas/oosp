import React from "react";
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip } from "recharts";

/**
 * Creates DL constructs distribution chart
 *
 * @param {Object} props - includes data to create chart from
 * @returns created chart
 * @component
 * @name DLConstructsDistribution
 */
export default function DLConstructsDistribution(props) {
  const data = [
    {
      name: "D",
      number: props.data.filter((item) => item.complexity.includes("D")).length,
    },
    {
      name: "E",
      number: props.data.filter((item) => item.complexity.includes("E")).length,
    },
    {
      name: "F",
      number: props.data.filter((item) => item.complexity.includes("F")).length,
    },
    {
      name: "AL",
      number: props.data.filter((item) => item.complexity.includes("AL"))
        .length,
    },
    {
      name: "C",
      number: props.data.filter((item) => item.complexity.includes("C")).length,
    },
    {
      name: "N",
      number: props.data.filter((item) => item.complexity.includes("N")).length,
    },
    {
      name: "O",
      number: props.data.filter((item) => item.complexity.includes("O")).length,
    },
    {
      name: "H",
      number: props.data.filter((item) => item.complexity.includes("H")).length,
    },
    {
      name: "I",
      number: props.data.filter((item) => item.complexity.includes("I")).length,
    },
    {
      name: "U",
      number: props.data.filter((item) => item.complexity.includes("U")).length,
    },
    {
      name: "Q",
      number: props.data.filter((item) => item.complexity.includes("Q")).length,
    },
    {
      name: "S",
      number: props.data.filter((item) => item.complexity.includes("S")).length,
    },
    {
      name: "R",
      number: props.data.filter((item) => item.complexity.includes("R")).length,
    },
  ];

  return (
    <>
      <div style={{ fontSize: "12px", marginLeft: "10px" }}>
        <h3 style={{ marginTop: 0, padding: 0 }}>
          Description logic constructs distribution
        </h3>
        <h4>X axis:Description logic constructs</h4>
        <h4>Y axis:number of ontologies</h4>
      </div>
      <BarChart
        width={800}
        height={400}
        data={data}
        margin={{
          top: 5,
          right: 30,
          left: 20,
          bottom: 5,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" interval={0} />
        <YAxis />
        <Tooltip />
        <Bar dataKey="number" fill="#82ca9d" />
      </BarChart>
      <div style={{ fontSize: "12px", marginLeft: "10px" }}>
        Legend:
        <ul>
          <li>'D'='Use of datatype properties, data values or data types.'</li>
          <li>'E'='Full existential qualification.'</li>
          <li>'F'='Functional properties.'</li>
          <li>'AL'='Attributive language.'</li>
          <li>'C'='Complex concept negation.'</li>
          <li>'N'='Cardinality restrictions.'</li>
          <li>
            'O'='Nominals. (Enumerated classes of object value restrictions -
            owl:oneOf, owl:hasValue).'
          </li>
          <li>'H'='Role hierarchy (subproperties - rdfs:subPropertyOf).'</li>
          <li>'I'='Inverse properties.'</li>
          <li>'U'='Concept union.'</li>
          <li>'Q'='Qualified cardinality restrictions.'</li>
          <li>
            'S'='Attributive Language with Complements and transitive roles.'
          </li>
          <li>'R'='Complex role inclusion.'</li>
        </ul>
      </div>
    </>
  );
}
