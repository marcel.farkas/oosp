import React from "react";
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip } from "recharts";

/**
 * Creates domain/range definition distribution chart
 *
 * @param {Object} props - includes data to create chart from
 * @returns created chart
 * @component
 * @name DomainRangeDefinitionDistribution
 */
export default function DomainRangeDefinitionDistribution(props) {
  const data = [
    {
      name: "Object properties",
      number: props.data.filter((item) => item.object_properties > 0).length,
    },
    {
      name: "Datatype properties",
      number: props.data.filter((item) => item.datatype_properties > 0).length,
    },
    {
      name: "Anonymous as Domain",
      number: props.data.filter((item) => item.domainAnonymous > 0).length,
    },
    {
      name: "Class as Domain",
      number: props.data.filter((item) => item.domainClass > 0).length,
    },
    {
      name: "Anonymous as Range",
      number: props.data.filter((item) => item.rangeAnonymous > 0).length,
    },
    {
      name: "Class as Range",
      number: props.data.filter((item) => item.rangeClass > 0).length,
    },
  ];

  return (
    <>
      <div style={{ fontSize: "12px", marginLeft: "10px" }}>
        <h3 style={{ marginTop: 0, padding: 0 }}>
          Domain/Range definition distribution
        </h3>
        <h4>X axis:Domain/range or properties</h4>
        <h4>Y axis:number of ontologies</h4>
      </div>
      <BarChart
        width={800}
        height={400}
        data={data}
        margin={{
          top: 5,
          right: 30,
          left: 20,
          bottom: 70,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis
          dataKey="name"
          interval={0}
          angle={-45}
          textAnchor="end"
          fontSize="12px"
        />
        <YAxis />
        <Tooltip />
        <Bar dataKey="number" fill="#82ca9d" />
      </BarChart>
      <div style={{ fontSize: "12px", marginLeft: "10px" }}>
        Legend:
        <ul>
          <li>
            'Anonymous as Domain'='Anonymous class expression used as domain
            definition.'
          </li>
          <li>'Class as Domain'='Named class used as domain definition.'</li>
          <li>
            'Anonymous as Range'='Anonymous class expression used as range
            definition.'
          </li>
          <li>'Class as Range'='Named class used as range definition.'</li>
        </ul>
      </div>
    </>
  );
}
