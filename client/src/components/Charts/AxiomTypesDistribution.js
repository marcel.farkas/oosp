import React from "react";
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip } from "recharts";

/**
 * Creates axiom types distribution chart
 *
 * @param {Object} props - includes data to create chart from
 * @returns created chart
 * @component
 * @name AxiomTypesDistribution
 */
export default function AxiomTypesDistribution(props) {
  const data = [
    {
      name: "axDisjoint",
      number: props.data.filter((item) => item.axDisjoint > 0).length,
    },
    {
      name: "axDisjointUnion",
      number: props.data.filter((item) => item.axDisjointUnion > 0).length,
    },
    {
      name: "axDifferent",
      number: props.data.filter((item) => item.axDifferent > 0).length,
    },
    {
      name: "axTransitiveProperty",
      number: props.data.filter((item) => item.axTransitiveProperty > 0).length,
    },
    {
      name: "axSymmetricProperty",
      number: props.data.filter((item) => item.axSymmetricProperty > 0).length,
    },
    {
      name: "axFunctionalDProperty",
      number: props.data.filter((item) => item.axFunctionalDProperty > 0)
        .length,
    },
    {
      name: "axFunctionalOProperty",
      number: props.data.filter((item) => item.axFunctionalOProperty > 0)
        .length,
    },
    {
      name: "axEquivalentClasses",
      number: props.data.filter((item) => item.axEquivalentClasses > 0).length,
    },
    {
      name: "axInverseOProperties",
      number: props.data.filter((item) => item.axInverseOProperties > 0).length,
    },
    {
      name: "axInverseFunctionalOProperties",
      number: props.data.filter(
        (item) => item.axInverseFunctionalOProperties > 0
      ).length,
    },
    {
      name: "axSame",
      number: props.data.filter((item) => item.axSame > 0).length,
    },
    {
      name: "axAnnotationAssertion",
      number: props.data.filter((item) => item.axAnnotationAssertion > 0)
        .length,
    },
    {
      name: "axClassAssertion",
      number: props.data.filter((item) => item.axClassAssertion > 0).length,
    },
    {
      name: "axEquivalentDataProperties",
      number: props.data.filter((item) => item.axEquivalentDataProperties > 0)
        .length,
    },
    {
      name: "axEquivalentObjectProperties",
      number: props.data.filter((item) => item.axEquivalentObjectProperties > 0)
        .length,
    },
    {
      name: "axDataPropertyDomain",
      number: props.data.filter((item) => item.axDataPropertyDomain > 0).length,
    },
    {
      name: "axDataPropertyRange",
      number: props.data.filter((item) => item.axDataPropertyRange > 0).length,
    },
    {
      name: "axObjectPropertyDomain",
      number: props.data.filter((item) => item.axObjectPropertyDomain > 0)
        .length,
    },
    {
      name: "axObjectPropertyRange",
      number: props.data.filter((item) => item.axObjectPropertyRange > 0)
        .length,
    },
    {
      name: "axDataPropertyAssertion",
      number: props.data.filter((item) => item.axDataPropertyAssertion > 0)
        .length,
    },
    {
      name: "axObjectPropertyAssertion",
      number: props.data.filter((item) => item.axObjectPropertyAssertion > 0)
        .length,
    },
    {
      name: "axSubDataProperty",
      number: props.data.filter((item) => item.axSubDataProperty > 0).length,
    },
    {
      name: "axSubObjectProperty",
      number: props.data.filter((item) => item.axSubObjectProperty > 0).length,
    },
    {
      name: "axSubClassOf",
      number: props.data.filter((item) => item.axSubClassOf > 0).length,
    },
  ];

  return (
    <>
      <div style={{ fontSize: "12px", marginLeft: "10px" }}>
        <h3 style={{ marginTop: 0, padding: 0 }}>Axiom types distribution</h3>
        <h4>X axis:Axiom types</h4>
        <h4>Y axis:number of ontologies</h4>
      </div>
      <BarChart
        width={800}
        height={400}
        data={data}
        margin={{
          top: 5,
          right: 30,
          left: 20,
          bottom: 110,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis
          dataKey="name"
          angle={-45}
          interval={0}
          dy={0}
          fontSize="12px"
          textAnchor="end"
        />
        <YAxis />
        <Tooltip />
        <Bar dataKey="number" fill="#82ca9d" />
      </BarChart>
      <div style={{ fontSize: "12px", marginLeft: "10px" }}>
        Legend:
        <ul>
          <li>axEquivalentClasses=Number of equivalent classes axioms.</li>
          <li>axDisjoint=Number of disjoint classes axioms.</li>
          <li>
            axDisjointUnion=Number of axioms of union of classes, all of which
            are pairwise disjoint.
          </li>
          <li>axClassAssertion=Number of class assertions axioms.</li>
          <li>axDifferent=Number of DifferentIndividuals axioms.</li>
          <li>axSame=Number of SameAs axioms.</li>
          <li>
            axTransitiveProperty=Number of properties with transitive
            characteristics.
          </li>
          <li>
            axSymmetricProperty=Number of properties with symmetric
            characteristics.
          </li>
          <li>
            axFunctionalDProperty=Number of datatype properties with functional
            characteristics.
          </li>
          <li>
            axFunctionalOProperty=Number of object properties with functional
            characteristics.
          </li>
          <li>
            axInverseOProperties=Number of object properties with inverse
            characteristics.
          </li>
          <li>
            axInverseFunctionalOProperties=Number of object properties with
            inverse functional characteristics.
          </li>
          <li>
            axEquivalentDataProperties=Number of equivalent datatype properties
            axioms.
          </li>
          <li>
            axEquivalentObjectProperties=Number of equivalent object properties
            axioms.
          </li>
          <li>
            axDataPropertyDomain=Number of datatype properties with domain
            definition.
          </li>
          <li>
            axDataPropertyRange=Number of datatype properties with range
            definition.
          </li>
          <li>
            axObjectPropertyDomain=Number of object properties with domain
            definition.
          </li>
          <li>
            axObjectPropertyRange=Number of object properties with range
            definition.
          </li>
          <li>
            axDataPropertyAssertion=Number of data property assertion axioms.
          </li>
          <li>
            axObjectPropertyAssertion=Number of object property assertion
            axioms.
          </li>
          <li>axSubDataProperty=Number of data property subsumption axioms.</li>
          <li>
            axSubObjectProperty=Number of object property subsumption axioms.
          </li>
          <li>axSubClassOf=Number of class subsumption axioms.</li>
        </ul>
      </div>
    </>
  );
}
