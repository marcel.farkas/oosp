import React from "react";
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip } from "recharts";

/**
 * Creates annotation distribution chart
 *
 * @param {Object} props - includes data to create chart from
 * @returns created chart
 * @component
 * @name AnnotationsDistribution
 */
export default function AnnotationsDistribution(props) {
  const data = [
    {
      name: "Labels",
      number: props.data.filter((item) => item.labels > 0).length,
    },
    {
      name: "Comments",
      number: props.data.filter((item) => item.comments > 0).length,
    },
    {
      name: "seeAlso",
      number: props.data.filter((item) => item.seeAlso > 0).length,
    },
    {
      name: "isDefinedBy",
      number: props.data.filter((item) => item.isDefinedBy > 0).length,
    },
    {
      name: "Multilingual",
      number: props.data.filter((item) => item.multilinguality > 1).length,
    },
  ];

  return (
    <>
      <div style={{ fontSize: "12px", marginLeft: "10px" }}>
        <h3 style={{ marginTop: 0, padding: 0 }}>Annotations distribution</h3>
        <h4>X axis:Annotation or Multinguality</h4>
        <h4>Y axis:number of ontologies</h4>
      </div>
      <BarChart
        width={800}
        height={400}
        data={data}
        margin={{
          top: 5,
          right: 30,
          left: 20,
          bottom: 5,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" interval={0} />
        <YAxis />
        <Tooltip />
        <Bar dataKey="number" fill="#82ca9d" />
      </BarChart>
      <div style={{ fontSize: "12px", marginLeft: "10px" }}>
        Legend:
        <ul>
          <li>'Labels'='labels annotations'</li>
          <li>'Comments'='comments annotations'</li>
          <li>'seeAlso'='seeAlso annotations'</li>
          <li>'isDefinedBy'='isDefinedBy annotations'</li>
          <li>'Multilingual'='multilingual labels annotations'</li>
        </ul>
      </div>
    </>
  );
}
