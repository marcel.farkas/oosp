import React from "react";
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip } from "recharts";

/**
 * Creates axiom frequency distribution chart
 *
 * @param {Object} props - includes data to create chart from
 * @returns created chart
 * @component
 * @name AxiomFrequencyDistribution
 */
export default function AxiomFrequencyDistribution(props) {
  const data = [
    {
      name: "10",
      number: props.data.filter((item) => item.axiomCount < 10).length,
    },
    {
      name: "100",
      number: props.data.filter(
        (item) => item.axiomCount >= 10 && item.axiomCount < 100
      ).length,
    },
    {
      name: "1k",
      number: props.data.filter(
        (item) => item.axiomCount >= 100 && item.axiomCount < 1000
      ).length,
    },
    {
      name: "10k",
      number: props.data.filter(
        (item) => item.axiomCount >= 1000 && item.axiomCount < 10000
      ).length,
    },
    {
      name: "100k",
      number: props.data.filter(
        (item) => item.axiomCount >= 10000 && item.axiomCount < 100000
      ).length,
    },
    {
      name: "1M",
      number: props.data.filter((item) => item.axiomCount >= 100000).length,
    },
  ];

  return (
    <>
      <div style={{ fontSize: "12px", marginLeft: "10px" }}>
        <h3 style={{ marginTop: 0, padding: 0 }}>
          Axiom frequency distribution
        </h3>
        <h4>X axis:Axiom frequency</h4>
        <h4>Y axis:number of ontologies</h4>
      </div>
      <BarChart
        width={800}
        height={400}
        data={data}
        margin={{
          top: 5,
          right: 30,
          left: 0,
          bottom: 5,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" interval={0} />
        <YAxis />
        <Tooltip />
        <Bar dataKey="number" fill="#82ca9d" />
      </BarChart>
      <div style={{ fontSize: "12px", marginLeft: "10px" }}>
        Legend: 1k=1,000; 1M=1,000,000
      </div>
    </>
  );
}
