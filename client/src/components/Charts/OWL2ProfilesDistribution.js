import React from "react";
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip } from "recharts";

/**
 * Creates OWL2 profiles distribution chart
 *
 * @param {Object} props - includes data to create chart from
 * @returns created chart
 * @component
 * @name OWL2ProfilesDistribution
 */
export default function OWL2ProfilesDistribution(props) {
  const data = [
    {
      name: "OWL2",
      number: props.data.filter((item) => item.OWL2).length,
    },
    {
      name: "not OWL2",
      number: props.data.filter((item) => !item.OWL2).length,
    },
    {
      name: "OWL2EL only",
      number: props.data.filter(
        (item) => item.OWL2EL && !item.OWL2QL && !item.OWL2RL
      ).length,
    },
    {
      name: "OWL2EL all",
      number: props.data.filter((item) => item.OWL2EL).length,
    },
    {
      name: "OWL2QL only",
      number: props.data.filter(
        (item) => item.OWL2QL && !item.OWL2EL && !item.OWL2RL
      ).length,
    },
    {
      name: "OWL2QL all",
      number: props.data.filter((item) => item.OWL2QL).length,
    },
    {
      name: "OWL2RL only",
      number: props.data.filter(
        (item) => item.OWL2RL && !item.OWL2QL && !item.OWL2EL
      ).length,
    },
    {
      name: "OWL2RL all",
      number: props.data.filter((item) => item.OWL2RL).length,
    },
  ];

  return (
    <>
      <div style={{ fontSize: "12px", marginLeft: "10px" }}>
        <h3 style={{ marginTop: 0, padding: 0 }}>OWL2 profiles distribution</h3>
        <h4>X axis:OWL2 profiles</h4>
        <h4>Y axis:number of ontologies</h4>
      </div>
      <BarChart
        width={800}
        height={400}
        data={data}
        margin={{
          top: 5,
          right: 30,
          left: 20,
          bottom: 5,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" interval={0} fontSize="12px" />
        <YAxis />
        <Tooltip />
        <Bar dataKey="number" fill="#82ca9d" />
      </BarChart>
      <div style={{ fontSize: "12px", marginLeft: "10px" }}>
        Legend:
        <ul>
          <li>'OWL2'='in OWL 2 Full'</li>
          <li>
            'not OWL2'='not in OWL 2 Full, i.e. it violates 'non absolute IRIs,
            undeclared datatypes or use of defined datatypes in restrictions'.
          </li>
          <li>'OWL2EL only'='only in OWL 2 EL profile'</li>
          <li>'OWL2EL all'='in OWL 2 EL profile'</li>
          <li>'OWL2QL only'='only in OWL 2 QL profile'</li>
          <li>'OWL2QL all'=''in OWL 2 QL profile </li>
          <li>'OWL2RL only'='only in OWL 2 RL profile'</li>
          <li>'OWL2RL all'='in OWL 2 RL profile'</li>
        </ul>
      </div>
    </>
  );
}
