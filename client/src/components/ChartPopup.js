import React from "react";
import Popup from "reactjs-popup";
import Button from "react-bootstrap/Button";

/**
 * Creates a popup where a chart will be shown
 *
 * @component
 * @param {Object} props - Chart to be shown and its name
 */
const ChartPopup = (props) => (
  <Popup trigger={<Button> {props.name} </Button>} modal nested lockScroll>
    {(close) => (
      <div
        style={{
          backgroundColor: "white",
          width: "850px",
          height: "650px",
          overflowY: "auto",
          border: "2px solid black",
        }}
      >
        <button
          style={{
            cursor: "pointer",
            position: "absolute",
            display: "block",
            padding: "2px 5px",
            lineHeight: "20px",
            right: "-10px",
            top: "-10px",
            fontSize: "24px",
            background: "#ffffff",
            borderRadius: "18px",
            border: "1px solid black",
          }}
          onClick={close}
        >
          &times;
        </button>
        {props.chart}
      </div>
    )}
  </Popup>
);

export default ChartPopup;
