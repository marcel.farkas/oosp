import React, { Component } from "react";
import Form from "react-bootstrap/Form";

/**
 * Generates a dropdown list of resources
 *
 * @component
 */
class ResourceFilter extends Component {
  /** @constructor */
  constructor(props) {
    super(props);
    this.state = {
      resourceFilter: [],
    };
  }

  /**
   * Fetches resources from a database
   *
   * @method
   */
  async componentDidMount() {
    try {
      const res = await fetch(`/resourceFilter`, {
        method: "GET",
      });
      const data = await res.json();
      this.setState({ resourceFilter: data });
    } catch (e) {
      console.error(e);
    }
  }

  /**
   * Passes a selected resource to a parent
   *
   * @param {Object} evt - onChange event of a select element
   * @method
   */
  handleSelectChange = (evt) => {
    const resource = evt.target.value;
    this.props.selectionChanged(resource);
  };

  /**
   * Renders a dropdown with resources
   *
   * @method
   */
  render() {
    return (
      <div className="ontology-pool-selection">
        <Form.Label style={{ display: "inline", marginRight: "10px" }}>
          Ontology pool:
        </Form.Label>
        <Form.Control
          as="select"
          className="ontology-pool-select"
          style={{ width: "15em", display: "inline" }}
          onChange={this.handleSelectChange}
        >
          <option key="all" value="all">
            All ontologies in OOSP
          </option>
          {this.state.resourceFilter
            .sort((a, b) => a.DISTINCT > b.DISTINCT)
            .map((item) => {
              if (item.DISTINCT) {
                return <option key={item.DISTINCT}>{item.DISTINCT}</option>;
              }
              return false;
            })}
        </Form.Control>
      </div>
    );
  }
}

export default ResourceFilter;
