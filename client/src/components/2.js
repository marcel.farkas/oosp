import React, { Component } from "react";
import ResourceFilter from "./ResourceFilter";
import Button from "react-bootstrap/Button";
import OntologiesTable from "./OntologiesTable";
import { Form } from "react-bootstrap";
import Modal from "react-bootstrap/Modal";

/**
 * Ontology Search Based on Lexical Tokens page
 *
 * @component
 */
class _2 extends Component {
  /** @constructor */
  constructor(props) {
    super(props);
    this.state = {
      resourceFilter: "all",
      word: "",
      matcher: "exact",
      entities: ["classes"],
      scope: ["localName"],
      data: [],
      showPopup: false,
      popupMessage: "",
      popupTitle: "",
    };

    this.tableRef = React.createRef();
  }

  /**
   * Sets the browser tab name
   *
   * @method
   */
  componentDidMount() {
    document.title = "OOSP - Lexical Tokens";
  }

  /**
   * Scrolls to the table with ontology results
   *
   * @method
   */
  scrollToTable = () =>
    this.tableRef.current.scrollIntoView({ behavior: "smooth" });

  /**
   * Hides popups
   *
   * @method
   */
  hidePopup = () => {
    this.setState({ showPopup: false });
  };

  /**
   * Saves the state of a selected resource when the resource is changed
   *
   * @method
   * @param {String} resource - Selected resource
   */
  selectionChanged = (resource) => {
    this.setState({ resourceFilter: resource });
  };

  /**
   * Saves the state of a word to look for when input value changes
   *
   * @method
   * @param {Object} evt - onChange event of input field
   */
  wordChanged = (evt) => {
    const word = evt.target.value;
    this.setState({ word: word });
  };

  /**
   * Saves the selected matcher based on which radio button is pressed
   *
   * @param {Object} evt - onChange event of radio button
   * @method
   */
  matcherChanged = (evt) => {
    const matcher = evt.target.value;
    this.setState({ matcher: matcher });
  };

  /**
   * Saves the state of a check box from entities or scopes when it is checked/unchecked
   *
   * @param {Object} evt - onChange event of checkbox
   * @method
   */
  checkboxChanged = (evt) => {
    const name = evt.target.name;
    const value = evt.target.value;
    const checked = evt.target.checked;
    if (checked) {
      this.setState({ [name]: [...this.state[name], value] });
    } else {
      let state = { ...this.state };
      const index = state[name].findIndex((item) => item === value);
      if (index > -1) {
        state[name].splice(index, 1);
        this.setState(state);
      }
    }
  };

  /**
   * Fetches ontologies based on selected filters
   *
   * @method
   * @async
   */
  getData = async () => {
    const { resourceFilter, word, matcher, entities, scope } = {
      ...this.state,
    };

    if (!word) {
      this.setState({
        showPopup: true,
        popupTitle: "Error",
        popupMessage: "Word can not be empty",
      });
      return;
    }

    if (!entities.length) {
      this.setState({
        showPopup: true,
        popupTitle: "Error",
        popupMessage: "At least one entity needs to be selected",
      });
      return;
    }

    if (!scope.length) {
      this.setState({
        showPopup: true,
        popupTitle: "Error",
        popupMessage: "At least one scope needs to be selected",
      });
      return;
    }

    try {
      const res = await fetch(`/lexicalTokens`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Accept: "application/zip",
        },
        body: JSON.stringify({
          resource: resourceFilter,
          word: word,
          matcher: matcher,
          entities: entities,
          scope: scope,
        }),
      });
      const data = await res.json();
      this.setState({ data: data }, () => {
        if (data.length) this.scrollToTable();
      });
    } catch (e) {
      console.error(e);
    }
  };

  /**
   * Renders Ontology Search Based on Lexical Tokens component
   *
   * @method
   */
  render() {
    const {
      data,
      word,
      matcher,
      entities,
      scope,
      resourceFilter,
      showPopup,
      popupTitle,
      popupMessage,
    } = this.state;

    return (
      <>
        <Modal show={showPopup} onHide={this.hidePopup}>
          <Modal.Header closeButton>
            <Modal.Title>{popupTitle}</Modal.Title>
          </Modal.Header>
          <Modal.Body>{popupMessage}</Modal.Body>
        </Modal>
        <h3>Ontology Search Based on Lexical Tokens</h3>
        <ResourceFilter selectionChanged={this.selectionChanged} />
        <div className="fulltext-search-controls">
          <hr />
          <b>Word:</b>
          <Form.Control
            as="input"
            className="word-input"
            name="word"
            onChange={this.wordChanged}
          />
          <hr />
          <b>Matcher:</b>
          <Form.Check
            inline
            type="radio"
            name="matcher"
            label="Exact match"
            value="exact"
            defaultChecked={true}
            onChange={this.matcherChanged}
          />
          {/* <label htmlFor="exact"></label> */}
          <Form.Check
            inline
            type="radio"
            name="matcher"
            label="Word match (fulltext)"
            value="fulltext"
            onChange={this.matcherChanged}
          />
          {/* <label htmlFor="fulltetx"></label> */}
          <hr />
          <b>Entities:</b>
          <Form.Check
            inline
            type="checkbox"
            name="entities"
            label="Classes"
            value="classes"
            defaultChecked={true}
            onChange={this.checkboxChanged}
          />
          {/* <label htmlFor="classes">Classes</label> */}
          <Form.Check
            inline
            type="checkbox"
            name="entities"
            label="Properties"
            value="properties"
            onChange={this.checkboxChanged}
          />
          {/* <label htmlFor="properties">Properties</label> */}
          <Form.Check
            inline
            type="checkbox"
            name="entities"
            label="Individuals"
            value="individuals"
            onChange={this.checkboxChanged}
          />
          {/* <label htmlFor="individuals">Individuals</label> */}
          <hr />
          <b>Scope:</b>
          <Form.Check
            inline
            type="checkbox"
            name="scope"
            label="Local name"
            value="localName"
            defaultChecked={true}
            onChange={this.checkboxChanged}
          />
          {/* <label htmlFor="localName">Local name</label> */}
          <Form.Check
            inline
            type="checkbox"
            name="scope"
            label="Label"
            value="label"
            onChange={this.checkboxChanged}
          />
          {/* <label htmlFor="label">Label</label> */}
          <Form.Check
            inline
            type="checkbox"
            name="scope"
            label="Comment"
            value="comment"
            onChange={this.checkboxChanged}
          />
          {/* <label htmlFor="comment">Comment</label> */}
          <hr />
        </div>
        <Button onClick={this.getData} className="get-data-btn">
          Get Ontology Set according to Full Text Search
        </Button>
        <OntologiesTable
          data={data}
          page="2"
          word={word}
          matcher={matcher}
          entities={entities}
          scope={scope}
          resourceFilter={resourceFilter}
          tableRef={this.tableRef}
        />
      </>
    );
  }
}

export default _2;
