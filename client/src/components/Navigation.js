import React, { Component } from "react";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import NavDropdown from "react-bootstrap/NavDropdown";
import { withRouter } from "react-router";

/**
 * Creates a navigation component with router
 *
 * @component
 */
class Navigation extends Component {
  render() {
    const { location } = this.props;

    return (
      <Navbar collapseOnSelect expand="md" bg="primary" variant="dark">
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav
            className="mr-auto"
            activeKey={location.pathname}
            defaultActiveKey="/"
          >
            <Nav.Link href="/" className="nav-item">
              Home
            </Nav.Link>
            <NavDropdown
              id="nav-dropdown"
              className="nav-item"
              title="Start OOSP"
              active={
                location.pathname.toUpperCase() === "/INTERVALSOFMETRICS" ||
                location.pathname.toUpperCase() === "/LEXICALTOKENS"
              }
            >
              <NavDropdown.Item href="/IntervalsOfMetrics">
                Ontology Search based on Given Exact Intervals of Metrics
              </NavDropdown.Item>
              <NavDropdown.Item href="/LexicalTokens">
                Ontology Search based on Lexical Tokens
              </NavDropdown.Item>
            </NavDropdown>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}

export default withRouter(Navigation);
