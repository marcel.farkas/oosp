import React, { Component } from "react";
import EntityMetricsFilter from "./EntityMetricsFilters";
import Tabs from "./Tabs";
import Button from "react-bootstrap/Button";
import AxiomMetricsFilters from "./AxiomMetricsFilters";
import ClassExpressionTypeMetricsFilters from "./ClassExpressionTypeMetricsFilters";
import TaxonomyMetricsFilters from "./TaxonomyMetricsFilters";
import OWL2ProfilesAndReasoning from "./OWL2ProfilesAndReasoningMetricsFilters";
import OntologiesTable from "./OntologiesTable";
import AnnotationMetricsFilters from "./AnnotationMetricsFilters";
import DetailMetricsFilters from "./DetailMetricsFilters";
import Modal from "react-bootstrap/Modal";
import ResourceFilter from "./ResourceFilter";

/**
 * Ontology Search based on Given Exact Intervals of Metrics page
 *
 * @component
 */
class _1 extends Component {
  /** @constructor */
  constructor(props) {
    super(props);
    this.state = {
      filters: { selectedResource: "all" },
      data: [],
      showPopup: false,
      popupMessage: "",
      popupTitle: "",
      storageCodes: [],
      ontologiesStats: [],
    };

    this.tableRef = React.createRef();
  }

  /**
   * Fetches initial statistics for metrics and sets the browser tab name
   *
   * @method
   * @async
   */
  async componentDidMount() {
    document.title = "OOSP - Intervals of Metrics";
    let data = [];
    try {
      const res = await fetch(`/getOntolologiesStats/all`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
      });
      data = await res.json();
      this.setState({ ontologiesStats: data });
    } catch (e) {
      console.error(e);
    }
  }

  /**
   * Scrolls to the table with ontology results
   *
   * @method
   */
  scrollToTable = () =>
    this.tableRef.current.scrollIntoView({ behavior: "smooth" });

  /**
   * Updates filter array according to changes in filters
   *
   * @method
   * @param {Object} filter - filter to be updated
   */
  updateFilters = (filter) => {
    let isAllowed = true;

    for (const item in filter) {
      if (
        filter[item] !== "" &&
        (Number(parseFloat(filter[item])) !== parseFloat(filter[item]) ||
          filter[item] < 0)
      ) {
        isAllowed = false;
        break;
      }
    }
    if (isAllowed) {
      this.setState({ filters: { ...this.state.filters, ...filter } });
    } else {
      this.setState({
        showPopup: true,
        popupTitle: "Wrong value",
        popupMessage: `Please enter number higher than zero or equal to zero.`,
      });
    }
  };

  /**
   * Fetches ontologies based on selected filters
   *
   * @method
   * @async
   */
  getData = async () => {
    try {
      const res = await fetch("/ontologies", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
        body: JSON.stringify(this.state.filters),
      });
      const data = await res.json();
      this.setState(
        {
          data: data,
        },
        () => {
          if (data.length) this.scrollToTable();
        }
      );
    } catch (e) {
      console.log(e);
    }
  };

  /**
   * Fetches metrics statistics when a resource is changed
   *
   * @method
   * @param {String} resource - Selected resource
   * @async
   */
  handleSelectChange = async (resource) => {
    let data = [];
    try {
      const res = await fetch(`/getOntolologiesStats/${resource}`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
      });
      data = await res.json();
    } catch (e) {
      console.log(e);
    }
    this.setState({
      filters: { ...this.state.filters, selectedResource: resource },
      ontologiesStats: data,
    });
  };

  /**
   * Hides popups
   *
   * @method
   */
  hidePopup = () => {
    this.setState({ showPopup: false });
  };

  /**
   * Resets all filters (except resource)
   *
   * @method
   */
  resetFilters = () => {
    const resource = this.state.filters.selectedResource;
    this.setState({
      filters: { selectedResource: resource },
      data: [],
      randomOntologiesNumber: "",
    });
  };

  /**
   * Renders Ontology Search based on Given Exact Intervals of Metrics component
   *
   * @method
   */
  render() {
    const {
      filters,
      data,
      showPopup,
      popupMessage,
      popupTitle,
      ontologiesStats,
    } = this.state;

    return (
      <div>
        <Modal show={showPopup} onHide={this.hidePopup}>
          <Modal.Header closeButton>
            <Modal.Title>{popupTitle}</Modal.Title>
          </Modal.Header>
          <Modal.Body>{popupMessage}</Modal.Body>
          {/* <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>Close</Button>
          <Button variant="primary" onClick={handleClose}>
            Save Changes
          </Button>
        </Modal.Footer> */}
        </Modal>
        <h3>Ontology Search based on Given Exact Intervals of Metrics</h3>
        <ResourceFilter selectionChanged={this.handleSelectChange} />
        <br />
        <Tabs>
          <div label="Entity">
            <EntityMetricsFilter
              stats={ontologiesStats}
              savedFilters={filters}
              updateFilters={this.updateFilters}
            />
          </div>
          <div label="Axiom">
            <AxiomMetricsFilters
              stats={ontologiesStats}
              savedFilters={filters}
              updateFilters={this.updateFilters}
            />
          </div>
          <div label="Class expression type">
            <ClassExpressionTypeMetricsFilters
              stats={ontologiesStats}
              savedFilters={filters}
              updateFilters={this.updateFilters}
            />
          </div>
          <div label="Taxonomy">
            <TaxonomyMetricsFilters
              stats={ontologiesStats}
              savedFilters={filters}
              updateFilters={this.updateFilters}
            />
          </div>
          <div label="OWL2 profiles and reasoning">
            <OWL2ProfilesAndReasoning
              stats={ontologiesStats}
              savedFilters={filters}
              updateFilters={this.updateFilters}
            />
          </div>
          <div label="Annotation">
            <AnnotationMetricsFilters
              stats={ontologiesStats}
              savedFilters={filters}
              updateFilters={this.updateFilters}
            />
          </div>
          <div label="Detail">
            <DetailMetricsFilters
              stats={ontologiesStats}
              savedFilters={filters}
              updateFilters={this.updateFilters}
            />
          </div>
        </Tabs>
        <div className="filter-btn-group">
          <Button onClick={this.getData}>
            Get Ontology Set according to Current Ontology Criteria Selection
          </Button>

          <Button
            variant="danger"
            onClick={this.resetFilters}
            className="reset-filters-btn"
          >
            Reset
          </Button>
        </div>

        <br />

        <OntologiesTable
          data={data}
          page="1"
          filters={filters}
          tableRef={this.tableRef}
        />
      </div>
    );
  }
}

export default _1;
