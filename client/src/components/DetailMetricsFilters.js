import React, { Component } from "react";
import { Form } from "react-bootstrap";
import Table from "react-bootstrap/Table";

/**
 * Detail filters
 *
 * @component
 */
class DetailMetricsFilters extends Component {
  /**
   * Passes the changed filter to a parent
   *
   * @param {Object} evt - onChange event of input field
   *
   * @method
   */
  handleFilterChange = (evt) => {
    this.props.updateFilters({ [evt.target.name]: evt.target.value });
  };

  /**
   * Renders detail filters table
   *
   * @method
   */
  render() {
    const { savedFilters, stats } = this.props;
    const filterNames = [
      "domainAnonymous",
      "domainClass",
      "maxClassInHierarchy",
      "avgClassInHierarchy",
      "rangeAnonymous",
      "rangeClass",
      "maxRangeClassInHierarchy",
      "avgRangeClassInHierarchy",
      "classesMaxReusedDomainRange",
      "classesMoreThanOnceDomainRange",
      "classesExensional",
      "maxInstancesInClassesExtensional",
      "avgInstancesInClassesExtensional",
    ];
    return (
      <div className="filterTable-wrapper">
        <Table className="filterTable" bordered>
          <thead>
            <tr>
              <th>Metrics</th>
              <th>Ontology Ratio</th>
              <th>N/A</th>
              <th>Median</th>
              <th>Average</th>
              <th>St. dev.</th>
              <th>Max</th>
              <th>Min Value</th>
              <th>Max Value</th>
            </tr>
          </thead>
          <tbody>
            {filterNames.map((filterName) => {
              return (
                <tr key={filterName}>
                  <td>{filterName}:</td>
                  {stats?.stats?.length ? (
                    stats.stats
                      .filter((item) => item.metrics === filterName)
                      .map((stat) => {
                        return (
                          <React.Fragment key={stat.metrics}>
                            <td>
                              {(
                                Math.round(
                                  ((stat.nonzero * 100) /
                                    stats.numOfOntologies[0].numOfOntologies +
                                    Number.EPSILON) *
                                    10
                                ) / 10
                              ).toFixed(1) + "%" ?? ""}
                            </td>
                            <td>
                              {(
                                Math.round(
                                  ((stat.unknown * 100) /
                                    stats.numOfOntologies[0].numOfOntologies +
                                    Number.EPSILON) *
                                    10
                                ) / 10
                              ).toFixed(1) + "%" ?? ""}
                            </td>
                            <td>
                              {(
                                Math.round(
                                  (stat.median + Number.EPSILON) * 10
                                ) / 10
                              ).toFixed(1) ?? ""}
                            </td>
                            <td>{stat.average ?? ""}</td>
                            <td>{stat.stdev ?? ""}</td>
                            <td>{stat.max ?? ""}</td>
                          </React.Fragment>
                        );
                      })
                  ) : (
                    <>
                      <td>N/A</td>
                      <td>N/A</td>
                      <td>N/A</td>
                      <td>N/A</td>
                      <td>N/A</td>
                      <td>N/A</td>
                    </>
                  )}
                  <td>
                    <Form.Control
                      as="input"
                      name={filterName + "_min"}
                      onChange={this.handleFilterChange}
                      value={savedFilters[filterName + "_min"] || ""}
                    />
                  </td>
                  <td>
                    <Form.Control
                      as="input"
                      name={filterName + "_max"}
                      onChange={this.handleFilterChange}
                      value={savedFilters[filterName + "_max"] || ""}
                    />
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </div>
    );
  }
}

export default DetailMetricsFilters;
