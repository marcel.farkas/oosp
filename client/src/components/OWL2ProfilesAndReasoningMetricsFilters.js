import React, { Component } from "react";
import { Form } from "react-bootstrap";
import Table from "react-bootstrap/Table";

/**
 * OWL2 profiles and reasoning filters
 *
 * @component
 */
class OWL2ProfilesAndReasoning extends Component {
  /**
   * Passes the changed filter to a parent
   *
   * @param {Object} evt - onChange event of input field
   *
   * @method
   */
  handleFilterChange = (evt) => {
    this.props.updateFilters({ [evt.target.name]: evt.target.value });
  };

  /**
   * Renders OWL2 profiles and reasoning filters table
   *
   * @method
   */
  render() {
    const { savedFilters, stats } = this.props;
    const filterNames = [
      "OWL2",
      "OWL2DL",
      "OWL2EL",
      "OWL2QL",
      "OWL2RL",
      "consistent",
      "numberUnsatClasses",
    ];
    return (
      <div className="filterTable-wrapper">
        <Table className="filterTable" bordered>
          <tbody>
            <tr>
              <th>Metrics</th>
              <th>Ontology Ratio</th>
              <th>N/A</th>
              <th>Median</th>
              <th>Average</th>
              <th>St. dev.</th>
              <th>Max</th>
              <th>Min Value</th>
              <th>Max Value</th>
            </tr>
            {filterNames.map((filterName) => {
              return (
                <tr key={filterName}>
                  <td>{filterName}:</td>
                  {stats?.stats?.length ? (
                    stats.stats
                      .filter((item) => item.metrics === filterName)
                      .map((stat) => {
                        return (
                          <React.Fragment key={stat.metrics}>
                            <td>
                              {(
                                Math.round(
                                  ((stat.nonzero * 100) /
                                    stats.numOfOntologies[0].numOfOntologies +
                                    Number.EPSILON) *
                                    10
                                ) / 10
                              ).toFixed(1) + "%" ?? ""}
                            </td>
                            <td>
                              {(
                                Math.round(
                                  ((stat.unknown * 100) /
                                    stats.numOfOntologies[0].numOfOntologies +
                                    Number.EPSILON) *
                                    10
                                ) / 10
                              ).toFixed(1) + "%" ?? ""}
                            </td>
                            <td>
                              {(
                                Math.round(
                                  (stat.median + Number.EPSILON) * 10
                                ) / 10
                              ).toFixed(1) ?? ""}
                            </td>
                            <td>{stat.average ?? ""}</td>
                            <td>{stat.stdev ?? ""}</td>
                            <td>{stat.max ?? ""}</td>
                          </React.Fragment>
                        );
                      })
                  ) : (
                    <>
                      <td>N/A</td>
                      <td>N/A</td>
                      <td>N/A</td>
                      <td>N/A</td>
                      <td>N/A</td>
                      <td>N/A</td>
                    </>
                  )}
                  <td>
                    {filterName === "numberUnsatClasses" ? (
                      <Form.Control
                        as="input"
                        name={filterName + "_min"}
                        onChange={this.handleFilterChange}
                        value={savedFilters[filterName + "_min"] || ""}
                      />
                    ) : (
                      <Form.Control
                        as="select"
                        name={filterName}
                        onChange={this.handleFilterChange}
                        value={savedFilters[filterName] || ""}
                      >
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                        <option value="">---</option>
                      </Form.Control>
                    )}
                  </td>
                  <td>
                    {filterName === "numberUnsatClasses" ? (
                      <Form.Control
                        as="input"
                        name={filterName + "_max"}
                        onChange={this.handleFilterChange}
                        value={savedFilters[filterName + "_max"] || ""}
                      />
                    ) : (
                      "---"
                    )}
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </div>
    );
  }
}

export default OWL2ProfilesAndReasoning;
