import "./App.css";
import Home from "./components/Home";
import _1 from "./components/1";
import _2 from "./components/2";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Navigation from "./components/Navigation";

function App() {
  return (
    <div className="App">
      <Router>
        <Navigation />
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/IntervalsOfMetrics" component={_1} />
          <Route path="/LexicalTokens" component={_2} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
