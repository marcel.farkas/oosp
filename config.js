const config = {
  APP_PORT: "8080",
  NODE_ENV: "production" /*"development"*/,
  DB_HOST: "127.0.0.1", //database host
  DB_USER: "root", //database user
  DB_PASS: "", //database password
  DB_NAME: "bp_ontologies", //database name
  ontologiesURL: "https://owl.vse.cz/ontologies", //location of ontologies (without trailing slash "/")
};

module.exports = config;
