const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const compression = require("compression");
const morgan = require("morgan");
const path = require("path");

const sequelize = require("./db");
const routes = require("./routes");
const config = require("./config");

const app = express();
app.set("trust proxy", "loopback");
app.use(morgan("dev"));
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(compression());
app.use(routes);

if (config.NODE_ENV === "production") {
  app.use(express.static(path.join(__dirname, "client", "build")));

  app.get("/*", (req, res) => {
    res.sendFile(path.join(__dirname, "client", "build", "index.html"));
  });
}

try {
  // sequelize.authenticate().then(() => {
  app.listen(config.APP_PORT || 8080, () => {
    console.log(`Server listening on port ${config.APP_PORT || 8080}`);
  });
  // });
} catch (err) {
  console.log("Unable to connect to the database: ", err);
}
